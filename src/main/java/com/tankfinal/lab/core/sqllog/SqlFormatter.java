package com.tankfinal.lab.core.sqllog;


import com.p6spy.engine.common.P6Util;
import com.p6spy.engine.spy.appender.MessageFormattingStrategy;
import lombok.extern.slf4j.Slf4j;

/**
 * 覆寫P6SPY輸出語句實現類
 *
 * @author Tank.Yang
 * @since 2020-06-22 14:28:32
 */
@Slf4j
public class SqlFormatter implements MessageFormattingStrategy {
    /**
     * 自定義格式化SQL輸出
     *
     * @param connectionId 連線置資料庫的ID
     * @param now          當前時間
     * @param elapsed      消耗秒數
     * @param category     SQL類別
     * @param prepared     SQL語句預編譯內容
     * @param sql          執行的SQL語句
     * @param url          資料庫地址
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-06-22 14:28:52
     */
    @Override
    public String formatMessage(int connectionId, String now, long elapsed, String category, String prepared, String sql, String url) {
        String logger = "connection " + connectionId + "|" + elapsed + "ms|" + P6Util.singleLine(sql);
        int overTime = 4000;
        if (elapsed > overTime) {
            log.warn(logger);
        }
        return logger;
    }
}
