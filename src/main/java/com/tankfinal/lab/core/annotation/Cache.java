package com.tankfinal.lab.core.annotation;


import com.tankfinal.lab.core.constant.Type;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 缓存注解只能作用在方法上
 * </p>
 *
 * @author Tank.Yang
 * @since 2019-03-15 10:51:38
 */
@SuppressWarnings("unused")
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Cache {

    /**
     * <p>
     * 緩存在Redis的Key
     * </p>
     *
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2019-03-15 10:53:04
     */
    String key();

    /**
     * <p>
     * 失效時間
     * </p>
     *
     * @return long
     * @author Tank.Yang
     * @since 2019-03-15 10:59:12
     */
    long timeout() default 0L;

    /**
     * <p>
     * 失效時間单位,默認為秒
     * </p>
     *
     * @return java.util.concurrent.TimeUnit
     * @author Tank.Yang
     * @since 2019-03-15 10:58:38
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * <p>
     * 緩存類型
     * </p>
     *
     * @return com.tankfinal.core.constant.enums.Type
     * @author Tank.Yang
     * @since 2019-03-18 16:33:37
     */
    Type type() default Type.DEFAULT;

    /**
     * <p>
     * 是否需要同步獲取,默認为非同步獲取
     * </p>
     *
     * @return boolean
     * @author Tank.Yang
     * @since 2019-03-15 10:54:15
     */
    boolean sync() default true;
}
