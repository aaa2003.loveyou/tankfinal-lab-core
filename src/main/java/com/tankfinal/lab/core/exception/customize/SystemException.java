package com.tankfinal.lab.core.exception.customize;


import com.tankfinal.lab.core.exception.model.SystemExceptionModel;

/**
 * 系統異常
 *
 * @author Tank.Yang
 * @since 2020-06-18 11:15:44
 */
@SuppressWarnings("unused")
public class SystemException extends AbstractException {

    private static final long serialVersionUID = 6505433366760915962L;

    public SystemException(String error) {
        super(error);
    }

    public SystemException(String error, String... data) {
        super(error, data);
    }

    public SystemException(Exception e) {
        super(e);
    }

    public SystemException(SystemExceptionModel e) {
        super(e.getCode(), e.getMessage());
    }

    public SystemException(SystemExceptionModel e, String... data) {
        super(e.getCode(), e.getMessage(), data);
    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

}
