package com.tankfinal.lab.core.exception.constant;

import com.tankfinal.lab.core.exception.model.ServiceExceptionModel;

/**
 * 業務類異常
 *
 * @author Tank.Yang
 * @since 2020-06-19 10:22:56
 */
public interface Services {


    /**
     * 用戶業務異常
     *
     * @author Tank.Yang
     * @since 2020-06-22 18:24:52
     */
    enum UserEx implements ServiceExceptionModel {
        /**
         * 認證過期
         */
        AUTHORIZATION_IS_EMPTY(2001, "認證已過期或尚未認證，請重新登入"),
        AUTHORIZATION_FAILED(2002, "認證失敗，請檢查帳號密碼"),
        ;

        private int code;
        private String message;

        UserEx(int code, String message) {
            this.code = code;
            this.message = message;
        }

        @Override
        public int getCode() {
            return this.code;
        }

        @Override
        public String getMessage() {
            return this.message;
        }
    }

}
