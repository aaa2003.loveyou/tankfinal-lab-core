package com.tankfinal.lab.core.exception.customize;

import com.tankfinal.lab.core.exception.model.ServiceExceptionModel;

/**
 * 業務異常
 *
 * @author Tank.Yang
 * @since 2020-06-16 13:49:28
 */
@SuppressWarnings("unused")
public class ServiceException extends AbstractException {

    private static final long serialVersionUID = -1622181561035493121L;

    public ServiceException(String error) {
        super(error);
    }

    public ServiceException(String error, String... data) {
        super(error, data);
    }

    public ServiceException(Exception e) {
        super(e);
    }

    public ServiceException(ServiceExceptionModel e) {
        super(e.getCode(), e.getMessage());
    }

    public ServiceException(ServiceExceptionModel e, String... data) {
        super(e.getCode(), e.getMessage(), data);
    }

    @Override
    public Throwable fillInStackTrace(){
        return this;
    }

}
