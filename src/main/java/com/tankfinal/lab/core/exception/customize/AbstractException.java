package com.tankfinal.lab.core.exception.customize;

import cn.hutool.core.util.ArrayUtil;
import com.tankfinal.lab.core.constant.Global;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * 自訂異常超類，所有自訂義異常必須集成該類
 *
 * @author Tank.Yang
 * @since 2020-06-16 13:50:11
 */
@Getter
@Setter
public abstract class AbstractException extends RuntimeException {

    private static final long serialVersionUID = 2231257163746369234L;

    /**
     * 堆棧信息陣列(數組)打印初始行數
     * */
    private static final int STACK_TRACE_LIMIT_FROM = 0;

    /**
     * 堆棧信息限制(數組)打印最終行數
     * */
    private static final int STACK_TRACE_LIMIT_TO = 10;

    /**
     * 錯誤碼
     */
    private int code;
    /**
     * 錯誤消息
     */
    private String error;
    /**
     * 錯誤資料
     * */
    private String[] data;

    AbstractException(String error) {
        super(error);
        this.code = Global.FAIL;
        this.error = error;
        this.printStackTrace(error);
    }

    AbstractException(Exception e) {
        super(e);
        this.code = Global.FAIL;
        this.error = e.getMessage();
        this.printStackTrace(e.getMessage());
    }

    AbstractException(int code, String error, String... data) {
        super(fillParam(error, data));
        this.code = code;
        this.data = data;
        this.error = fillParam(error, data);
        this.printStackTrace(fillParam(error, data));
    }

    AbstractException(String error, String... data) {
        super(fillParam(error, data));
        this.code = Global.FAIL;
        this.data = data;
        this.error = fillParam(error, data);
        this.printStackTrace(fillParam(error, data));
    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

    /**
     * 針對輸入字串做輸入參數補值
     * @author Tank.Yang
     * @since 2020-06-23 14:42:14
     * @param errorMsg 錯誤訊息
     * @param inArr 輸入陣列
     * @return java.lang.String
     */
    private static String fillParam(String errorMsg, final String... inArr){
        String[] symbols = new String[]{"{}", "%s"};
        for (String symbol : symbols) {
            errorMsg = replaceBySymbol(errorMsg, symbol, inArr);
        }
        return errorMsg;
    }

    /**
     * 置換字串
     *
     * @param errorMsg 錯誤訊息
     * @param symbol 目標記號
     * @param inArr 輸入陣列
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2022-05-27 11:19:08
     */
    private static String replaceBySymbol(String errorMsg, String symbol, String... inArr) {
        if (ArrayUtil.isEmpty(inArr)) {
            return errorMsg;
        }
        int symbolLen = StringUtils.countMatches(errorMsg , symbol);
        // 輸入陣列長度不足，補足空字串
        if (symbolLen > inArr.length) {
            for (int i = 0 ; i < symbolLen - inArr.length; i ++) {
                inArr = ArrayUtil.append(inArr, "");
            }
        }
        // 循環替換字串
        for (int i = 0 ; i < symbolLen ; i ++) {
            errorMsg = StringUtils.replaceOnce(errorMsg, symbol, inArr[i]);
        }
        return errorMsg;
    }

    /**
     * 打印堆棧信息
     *
     * @author Tank.Yang
     * @param errorMsg 錯誤信息
     * @since 2022-06-09 16:23:39
     */
    public void printStackTrace(String errorMsg) {
        LoggerFactory.getLogger(this.getClass()).error(errorMsg);
        StackTraceElement[] stackTraceElementArr = Arrays.copyOfRange((new Throwable()).getStackTrace(), STACK_TRACE_LIMIT_FROM, STACK_TRACE_LIMIT_TO);
        for (StackTraceElement stackTraceElement : stackTraceElementArr) {
            LoggerFactory.getLogger(this.getClass()).error(String.valueOf(stackTraceElement));
        }
    }
}
