package com.tankfinal.lab.core.exception.model;

/**
 * Service操作異常模型
 *
 * @author Tank.Yang
 * @since 2020-06-16 13:50:28
 */
public interface ServiceExceptionModel {

    /**
     * 獲取Code值
     *
     * @return int
     * @author Tank.Yang
     * @since 2020-06-16 13:50:37
     */
    int getCode();

    /**
     * 獲取信息
     *
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-06-16 13:50:42
     */
    String getMessage();
}
