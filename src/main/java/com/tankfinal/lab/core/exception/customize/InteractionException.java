package com.tankfinal.lab.core.exception.customize;


import com.tankfinal.lab.core.exception.model.InteractionExceptionModel;

/**
 * 系統交互異常(FeignClient)新架構使用
 *
 * @author Tank.Yang
 * @since 2020-06-18 11:15:44
 */
@SuppressWarnings("unused")
public class InteractionException extends AbstractException {

    private static final long serialVersionUID = 6505433366760915962L;

    public InteractionException(String error) {
        super(error);
    }

    public InteractionException(String error, String... data) {
        super(error, data);
    }

    public InteractionException(Exception e) {
        super(e);
    }

    public InteractionException(InteractionExceptionModel e) {
        super(e.getCode(), e.getMessage());
    }

    public InteractionException(InteractionExceptionModel e, String... data) {
        super(e.getCode(), e.getMessage(), data);
    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

}
