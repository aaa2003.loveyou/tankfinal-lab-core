package com.tankfinal.lab.core.exception.model;

/**
 * System操作異常模型
 *
 * @author Tank.Yang
 * @since 2020-06-18 11:16:08
 */
public interface SystemExceptionModel {

    /**
     * 獲取Code值
     *
     * @return int
     * @author Tank.Yang
     * @since 2020-06-18 11:16:27
     */
    int getCode();

    /**
     * 獲取信息
     *
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-06-18 11:16:35
     */
    String getMessage();
}
