package com.tankfinal.lab.core.exception.constant;


import com.tankfinal.lab.core.exception.model.SystemExceptionModel;

/**
 * 系統異常常量類
 *
 * @author Tank.Yang
 * @since 2020-06-18 11:17:59
 */
@SuppressWarnings("unused")
public interface Systems {

    /**
     * 系統異常消息
     *
     * @author Tank.Yang
     * @since 2020-06-18 11:21:11
     */
    enum SystemEx implements SystemExceptionModel {

        /**
         * 系统異常消息 定義
         */
        SYSTEM_ERROR(-1, "系統發生異常"),
        ANNOTATION_MUST_ON_METHOD(10001, "该注解只能使用在方法上"),
        ANNOTATION_CANNOT_ON_VOID_METHOD(10002, "該注解不能作用在方法返回值為void类型的方法上"),
        CACHE_KEY_CANNOT_BE_NULL(10003, "缓存Key不能為空"),
        ;

        private final int code;
        private final String message;

        SystemEx(int code, String message) {
            this.code = code;
            this.message = message;
        }

        @Override
        public int getCode() {
            return this.code;
        }

        @Override
        public String getMessage() {
            return this.message;
        }
    }

    /**
     * Http請求異常消息
     *
     * @author Tank.Yang
     * @since 2020-06-18 11:21:11
     */
    enum HttpEx implements SystemExceptionModel {
        /**
         * 系统異常消息 定義
         */
        HTTP_REQUEST_FAILED(1002, "Http請求失敗，錯誤碼:%s，訊息:%s"),
        PROXY_HOST_CANNOT_BE_EMPTY(1002, "代理主機地址不能為空"),
        PROXY_PORT_CANNOT_BE_EMPTY(1003, "代理主機端口不能為空"),
        ;

        private final int code;
        private final String message;

        HttpEx(int code, String message) {
            this.code = code;
            this.message = message;
        }

        @Override
        public int getCode() {
            return this.code;
        }

        @Override
        public String getMessage() {
            return this.message;
        }
    }
}

