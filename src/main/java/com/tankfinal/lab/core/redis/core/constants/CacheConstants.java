package com.tankfinal.lab.core.redis.core.constants;

import java.util.concurrent.TimeUnit;

/**
 * 缓存常量池
 *
 * @author Tank.Yang
 * @since 2019-05-30 01:42:41
 */
@SuppressWarnings("unused")
public interface CacheConstants {

    /**
     * 緩存默認閥值定義
     *
     * @author Tank.Yang
     * @since 2019-05-30 01:49:00
     */
    interface Default {
        /**
         * 默認的失效時間,默認不失效
         */
        Long TIME_OUT = -1L;
        /**
         * 默認的時間單位,分鐘
         */
        TimeUnit TIME_UNIT = TimeUnit.MINUTES;
    }
}
