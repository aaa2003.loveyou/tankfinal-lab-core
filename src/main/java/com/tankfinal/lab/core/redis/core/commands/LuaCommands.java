package com.tankfinal.lab.core.redis.core.commands;

import org.springframework.data.redis.core.script.DefaultRedisScript;

/**
 * <p>
 * Lua 脚本命令集
 * </p>
 *
 * @author Tank.Yang
 * @since 2019-05-29 00:22:37
 */
public interface LuaCommands {

    /**
     * execute最終調用的RedisConnection的eval方法將LUA脚本交给Redis服務端執行
     * 這個操作通過Lua 腳本執行setnx 設置鎖key
     */
    DefaultRedisScript<Long> LOCK_LUA_SCRIPT = new DefaultRedisScript<>(
            "if redis.call(\"setnx\", KEYS[1], KEYS[2]) == 1 then return redis.call(\"pexpire\", KEYS[1], KEYS[3]) else return 0 end"
            , Long.class
    );

    /**
     * 先判斷key值是否與傳入的requestId一致，如果一致則删除key，如果不一致返回-1表示key可能已經過期或被其他客户端占用，避免誤删,redis.call("del",keys);如果删除成功返回1
     */
    String UNLOCK_LUA_SCRIPT = "if redis.call(\"get\",KEYS[1]) == ARGV[1] then return redis.call(\"del\",KEYS[1]) else return -1 end";

    /**
     * NX： 表示只有當鎖定資源不存在的时候才能 SET 成功。利用 Redis 的原子性，保證了只有第一個請求的線程才能獲得鎖，而之後的所有線程在鎖定資源被釋放之前都不能獲得鎖。
     */
    String NX = "NX";

    /**
     * PX： expire 表示鎖定的資源的自動過期時間，單位是毫秒。具体過期時間根據實際場景而定
     */
    String PX = "PX";

}
