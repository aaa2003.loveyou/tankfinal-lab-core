package com.tankfinal.lab.core.redis.kit;

import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * Redis 套件類
 * </p>
 *
 * @author Tank.Yang
 * @since 2019-05-29 00:35:19
 */
@SuppressWarnings("unused")
public interface RedisKit<K, V> {


    /**
     * <p>
     * 獲取RedisTemplate操作實例
     * </P>
     *
     * @return org.springframework.data.redis.core.RedisTemplate
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    RedisTemplate getRedisTemplate();

    /**
     * <p>
     * 注入緩存
     * </p>
     *
     * @param k 緩存Key
     * @param v 緩存Value
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    void set(K k, V v);

    /**
     * <p>
     * 注入緩存,並設置超時時間
     * </p>
     *
     * @param k       緩存Key
     * @param v       緩存Value
     * @param timeout 超時時間
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    void set(final K k, final V v, final Long timeout);

    /**
     * <p>
     * 注入緩存,並設置超時時間
     * </p>
     *
     * @param k       緩存Key
     * @param v       緩存Value
     * @param timeout 超時時間,如果timeout為null或者time=0則不進行設置
     * @param unit    超時時間单位,如果unit為null,則默認為分鐘
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    void set(final K k, final V v, final Long timeout, final TimeUnit unit);

    /**
     * <p>
     * 獲取緩存
     * </p>
     *
     * @param k 緩存Key
     * @return V
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    V get(final K k);

    /**
     * <p>
     * 是否存在Key
     * </p>
     *
     * @param k 緩存Key
     * @return boolean
     * @author Tank.Yang
     * @since 2019-10-18 14:47:46
     */
    boolean hasKey(final K k);

    /**
     * <p>
     * 獲取多個Key的Value
     * </p>
     *
     * @param keys Key列表
     * @return java.util.List<V>
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    List<V> multiGet(final Set<K> keys);


    /**
     * <p>
     * 删除緩存
     * </p>
     *
     * @param k 緩存Key
     * @return boolean
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    boolean delete(final K k);

    /**
     * <p>
     * 根据Key的前缀删除Key
     * </p>
     *
     * @param p Key前缀
     * @return long
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    boolean deleteByPrefix(String p);

    /**
     * <p>
     * 给Map添加一個元素
     * </p>
     *
     * @param k k 緩存Key
     * @param f Map中的Key
     * @param v 值
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    void hSet(final K k, final String f, final V v);


    /**
     * <p>
     * 緩存一個Map
     * </p>
     *
     * @param k k 緩存Key
     * @param m 整個Map
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    void hPutAll(final K k, final Map<K, V> m);


    /**
     * <p>
     * 獲取Map中的某個元素
     * </p>
     *
     * @param k k 緩存Key
     * @param f Map中的Key
     * @return T
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    V hGet(final K k, final String f);

    /**
     * <p>
     * 獲取整個Map
     * </p>
     *
     * @param k 緩存Key
     * @return java.util.Map
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    Map entries(K k);


    /**
     * <p>
     * 獲取Map的元素個数
     * </p>
     *
     * @param k KEY
     * @return long
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    long hSize(final K k);

    /**
     * <p>
     * 獲取多個Key列表
     * </p>
     *
     * @param k    Map Key
     * @param keys Key列表
     * @return java.util.List<V>
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    List<V> multiGet(final K k, final List<K> keys);

    /**
     * <p>
     * 根據Key規則獲取所有key
     * </p>
     *
     * @param pattern 規則
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2019-07-01 19:50:32
     */
    Set<K> keys(K pattern);


}
