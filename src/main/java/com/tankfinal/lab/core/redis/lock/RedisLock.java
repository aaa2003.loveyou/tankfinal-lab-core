package com.tankfinal.lab.core.redis.lock;

import com.tankfinal.lab.core.redis.core.constants.LockConstants;
import io.lettuce.core.SetArgs;
import io.lettuce.core.api.async.RedisAsyncCommands;
import io.lettuce.core.cluster.api.async.RedisAdvancedClusterAsyncCommands;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 分布式鎖工具類
 * </p>
 *
 * @author Tank.Yang
 * @since 2019-11-14 11:27:48
 */
@Slf4j
public class RedisLock {

    private StringRedisTemplate redisTemplate;
    /**
     * 是否已加鎖標示
     */
    private volatile boolean isLock = false;
    /**
     * 加鎖key
     */
    private String key;
    /**
     * 失效時間
     */
    private int expire;
    /**
     * 時間單位
     */
    private TimeUnit timeUnit;
    /**
     * 重試次數
     */
    private int retry;
    /**
     * 重試間隔時間
     */
    private long interval;

    public RedisLock(StringRedisTemplate redisTemplate, String key) {
        this.redisTemplate = redisTemplate;
        this.key = key;
    }

    public RedisLock(StringRedisTemplate redisTemplate, String key, int expire) {
        this.redisTemplate = redisTemplate;
        this.key = key;
        this.expire = expire;
    }

    public RedisLock(StringRedisTemplate redisTemplate, String key, int expire, TimeUnit timeUnit) {
        this.redisTemplate = redisTemplate;
        this.key = key;
        this.expire = expire;
        this.timeUnit = timeUnit;
    }

    public RedisLock(StringRedisTemplate redisTemplate, String key, int expire, TimeUnit timeUnit, int retry) {
        this.redisTemplate = redisTemplate;
        this.key = key;
        this.expire = expire;
        this.timeUnit = timeUnit;
        this.retry = retry;
    }

    public RedisLock(StringRedisTemplate redisTemplate, String key, int expire, TimeUnit timeUnit, int retry, long interval) {
        this.redisTemplate = redisTemplate;
        this.key = key;
        this.expire = expire;
        this.timeUnit = timeUnit;
        this.retry = retry;
        this.interval = interval;
    }

    /**
     * <p>
     * 獲取加鎖Key
     * </p>
     *
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2019-07-23 16:25:12
     */
    private String buildKey() {
        return Optional
                .ofNullable(this.key)
                .filter(StringUtils::isNotBlank)
                .orElseThrow(() -> new NullPointerException("lock key can not be empty..."))
                .concat(LockConstants.KEY_SUFFIX);
    }

    /**
     * <p>
     * 獲取加鎖Key
     * </p>
     *
     * @param origin 原始key
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2019-07-23 16:25:12
     */
    private String buildKey(String origin) {
        return Optional
                .ofNullable(origin)
                .filter(StringUtils::isNotBlank)
                .orElseThrow(() -> new NullPointerException("lock key can not be empty..."))
                .concat(LockConstants.KEY_SUFFIX);
    }


    /**
     * <p>
     * 等待鎖,會重試直到重試次數结束,或者拿到鎖
     * </p>
     *
     * @return boolean
     * @author Tank.Yang
     * @since 2019-11-14 17:38:22
     */
    public boolean lock() {
        if (this.expire <= 0) {
            this.expire = LockConstants.Default.TIMEOUT;
        }
        if (null == this.timeUnit) {
            this.timeUnit = TimeUnit.MILLISECONDS;
        }
        if (this.retry <= 0) {
            this.retry = LockConstants.Default.RETRY;
        }
        if (this.interval <= 0L) {
            this.interval = LockConstants.Default.INTERVAL;
        }
        this.key = buildKey();
        return this.lock(this.key, this.expire, this.timeUnit, this.retry, this.interval);
    }

    /**
     * <p>
     * 不等待鎖,獲取鎖且不重試,如果未獲取到鎖,不等待和重試
     * </p>
     *
     * @return boolean
     * @author Tank.Yang
     * @since 2019-11-14 17:35:16
     */
    public boolean trylock() {
        if (this.expire <= 0) {
            this.expire = LockConstants.Default.TIMEOUT;
        }
        if (null == this.timeUnit) {
            this.timeUnit = TimeUnit.MILLISECONDS;
        }
        this.key = buildKey();
        return this.lockFun(this.key, this.expire, this.timeUnit);
    }


    /**
     * <p>
     * 等待鎖,會重試直到重試次數结束,或者拿到鎖
     * </p>
     *
     * @param key      加鎖Key
     * @param expire   失效時間
     * @param retry    重試次數
     * @param interval 重試間隔時間
     * @return boolean
     * @author Tank.Yang
     * @since 2019-05-29 01:31:26
     */
    private boolean lock(final String key, final int expire, final TimeUnit timeUnit, int retry, final long interval) {
        boolean lock;
        // 如果獲取鎖失败，按照傳入的重試次數进行重試,並遞減重試次數
        while (!(lock = this.lockFun(key, expire, timeUnit)) && retry-- > 0) {
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                return false;
            }
        }
        return lock;
    }


    /**
     * <p>
     * 加鎖函數
     * 注意一定要對key和value序列化,否則會報錯
     * io.lettuce.core.RedisException: io.netty.handler.codec.EncoderException: Cannot encode command. Please release the connection as the connection state may be out of sync.
     * </p>
     *
     * @param key    鎖Key
     * @param expire 超時時間
     * @return boolean
     * @author Tank.Yang
     * @since 2019-05-29 12:42:43
     */
    private synchronized boolean lockFun(final String key, final long expire, final TimeUnit timeUnit) {
        // 獲取超時時間
        final long time = this.getTime(expire, timeUnit);
        String result = null;
        try {
            result = this.redisTemplate.execute((RedisCallback<String>) connection -> {
                // 執行结果和請求ID,使用UUID生成,用來保證每次請求的獨立性
                String exec, reqId = String.valueOf(System.currentTimeMillis() + expire + 1);
                try {
                    Object nativeConnection = connection.getNativeConnection();
                    if (nativeConnection instanceof RedisAsyncCommands) {
                        // 單機模式
                        @SuppressWarnings("unchecked") RedisAsyncCommands<byte[], byte[]> command = (RedisAsyncCommands) nativeConnection;
                        exec = command
                                .getStatefulConnection()
                                .sync()
                                .set(key.getBytes(StandardCharsets.UTF_8), reqId.getBytes(StandardCharsets.UTF_8), SetArgs.Builder.nx().px(time));
                    } else if (nativeConnection instanceof RedisAdvancedClusterAsyncCommands) {
                        // 集群模式
                        @SuppressWarnings("unchecked") RedisAdvancedClusterAsyncCommands<byte[], byte[]> clusterAsyncCommands = (RedisAdvancedClusterAsyncCommands) nativeConnection;
                        exec = clusterAsyncCommands
                                .getStatefulConnection()
                                .sync()
                                .set(key.getBytes(StandardCharsets.UTF_8), reqId.getBytes(StandardCharsets.UTF_8), SetArgs.Builder.nx().px(time));
                    } else {
                        // 因為當前工程是以Springboot為使用對象而設計 對的是Springboot2.x版本,需要保證Springboot版本為2.x
                        throw new UnsupportedClassVersionError("Failed to convert nativeConnection. Is your SpringBoot main version > 2.0 ? Only lib:lettuce is supported.");
                    }
                } finally {
                    // 釋放資源
                    this.release(connection);
                }
                // 只有再設置成功的情况下保存本地变量
                if (LockConstants.SET_SUCCESS.equals(exec)) {
                    this.isLock = true;
                }
                return exec;
            });
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        // 返回结果不是OK,則加鎖失败
        return LockConstants.SET_SUCCESS.equals(result);
    }

    /**
     * <p>
     * 釋放鎖
     * 注意一定要對key和value序列化,否則會報錯
     * io.lettuce.core.RedisException: io.netty.handler.codec.EncoderException: Cannot encode command. Please release the connection as the connection state may be out of sync.
     * </p>
     *
     * @return boolean
     * @author Tank.Yang
     * @since 2019-05-29 01:32:24
     */
    public synchronized boolean unlock() {

        if (StringUtils.isBlank(this.key)) {
            log.debug("lock key can not be empty");
            return false;
        }

        // 釋放鎖的時候，有可能因為持鎖之後方法執行時間大於鎖的有效期，此時有可能已经被另外一個线程持有鎖，所以不能直接删除
        if (!this.isLock) {
            log.debug("current lock is no locked");
            return false;
        }

        Long result = null;
        try {
            result = this.redisTemplate.execute((RedisCallback<Long>) connection -> {
                Long exec;
                try {
                    Object nativeConnection = connection.getNativeConnection();
                    if (nativeConnection instanceof RedisAsyncCommands) {
                        // 單機模式
                        @SuppressWarnings("unchecked") RedisAsyncCommands<byte[], byte[]> command = (RedisAsyncCommands) nativeConnection;
                        exec = command
                                .getStatefulConnection()
                                .sync()
                                .del(key.getBytes(StandardCharsets.UTF_8));
                    } else if (nativeConnection instanceof RedisAdvancedClusterAsyncCommands) {
                        // 集群模式
                        @SuppressWarnings("unchecked") RedisAdvancedClusterAsyncCommands<byte[], byte[]> clusterAsyncCommands = (RedisAdvancedClusterAsyncCommands) nativeConnection;
                        exec = clusterAsyncCommands
                                .getStatefulConnection()
                                .sync()
                                .del(key.getBytes(StandardCharsets.UTF_8));
                    } else {
                        // 因為當前工程是以Springboot為使用對象而設計,針對的是Springboot2.x版本,需要保證Springboot版本為2.x
                        throw new UnsupportedClassVersionError("Failed to convert nativeConnection. Is your SpringBoot main version > 2.0 ? Only lib:lettuce is supported.");
                    }
                } finally {
                    // 釋放資源
                    this.release(connection);
                }
                return exec;
            });
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        // 返回结果不是OK,則加鎖失败
        return LockConstants.UNLOCK_SUCCESS.equals(result);
    }


    /**
     * <p>
     * 釋放資源,異步操作Redis,連接不能保持同步,必须釋放
     * </p>
     *
     * @param connection Redis 連接對象
     * @author Tank.Yang
     * @since 2019-05-29 17:24:45
     */
    private void release(RedisConnection connection) {
        try {
            Optional
                    .ofNullable(connection)
                    .filter(c -> !c.isClosed())
                    .ifPresent(RedisConnection::close);
        } catch (Exception e) {
            log.error("release connection fail.", e);
        }
    }

    /**
     * <p>
     * 根據時間單位取時間毫秒
     * </p>
     *
     * @param time     時間
     * @param timeUnit 時間單位
     * @return long 毫秒
     * @author Tank.Yang
     * @since 2019-07-23 16:58:27
     */
    private long getTime(final long time, final TimeUnit timeUnit) {
        switch (timeUnit) {
            case DAYS:
                return TimeUnit.DAYS.toMillis(time);
            case HOURS:
                return TimeUnit.HOURS.toMillis(time);
            case MINUTES:
                return TimeUnit.MINUTES.toMillis(time);
            default:
                return time;
        }
    }

    /**
     * <p>
     * 重新設置现有鎖的時間
     * </p>
     *
     * @param key      加鎖Key
     * @param time     延长時間
     * @param timeUnit 時間單位
     * @return boolean
     * @author Tank
     * @since 2019-10-23 14:46:38
     */
    public synchronized boolean resetTime(final String key, final int time, final TimeUnit timeUnit) {
        Boolean expire = this.redisTemplate.expire(buildKey(key), time, timeUnit);
        return null == expire ? false : expire;
    }

    /**
     * <p>
     * 取得鎖的剩余時間
     * </p>
     *
     * @param key 加鎖Key
     * @return long 毫秒
     * @author Tank
     * @since 2019-10-23 15:52:12
     */
    public synchronized long getExpire(final String key) {
        Long expire = this.redisTemplate.getExpire(buildKey(key));
        return null == expire ? 0L : expire;
    }
}
