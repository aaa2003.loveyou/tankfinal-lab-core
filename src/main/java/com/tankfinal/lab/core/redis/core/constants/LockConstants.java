package com.tankfinal.lab.core.redis.core.constants;

/**
 * <p>
 * 分布式鎖常量池
 * </p>
 *
 * @author Tank.Yang
 * @since 2019-05-29 01:57:32
 */
@SuppressWarnings("unused")
public interface LockConstants {

    /**
     * <p>
     * 鎖默認閥值定義
     * </p>
     *
     * @author Tank.Yang
     * @since 2019-05-29 20:54:05
     */
    interface Default {
        /**
         * 默認超時時間
         */
        int TIMEOUT = 30000;
        /**
         * 默認重試次數
         */
        int RETRY = 20000;
        /**
         * 默認重試間隔時間
         */
        long INTERVAL = 100;
    }

    /**
     * 使用RedisAsyncCommands 或 RedisAdvancedClusterAsyncCommands 執行set命令返回成功的標示
     */
    String SET_SUCCESS = "OK";
    /**
     * 釋放鎖失败
     */
    Long UNLOCK_FAIL = -1L;
    /**
     * 釋放鎖成功
     */
    Long UNLOCK_SUCCESS = 1L;
    /**
     * 加鎖key後輟
     */
    String KEY_SUFFIX = "_lock";
}
