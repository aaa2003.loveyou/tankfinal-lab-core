package com.tankfinal.lab.core.redis.kit.impl;

import com.tankfinal.lab.core.redis.core.constants.CacheConstants;
import com.tankfinal.lab.core.redis.kit.RedisKit;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * Redis 操作套件類
 * </p>
 *
 * @author Tank.Yang
 * @since 2019-05-29 00:35:19
 */
public class RedisKitImpl<K, V> implements RedisKit<K, V> {

    private RedisTemplate<K, V> redisTemplate;

    public RedisKitImpl(RedisTemplate<K, V> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public RedisTemplate<K, V> getRedisTemplate() {
        return this.redisTemplate;
    }


    /**
     * <p>
     * 注入緩存
     * </p>
     *
     * @param k 緩存Key
     * @param v 緩存Value
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    @Override
    public void set(K k, V v) {
        this.set(k, v, CacheConstants.Default.TIME_OUT);
    }

    /**
     * <p>
     * 設置緩存
     * </p>
     *
     * @param k       緩存Key
     * @param v       緩存Value
     * @param timeout 超時時间(minutes)
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    @Override
    public void set(K k, V v, Long timeout) {
        this.set(k, v, timeout, CacheConstants.Default.TIME_UNIT);
    }

    /**
     * <p>
     * 注入緩存,並設置超時時间
     * </p>
     *
     * @param k       緩存Key
     * @param v       緩存Value
     * @param timeout 超時時間,如果timeout為CacheConstants.Default.ZERO或者time=0則不進行設置
     * @param unit    超時時間單位,如果unit為CacheConstants.Default.ZERO,則默認毫秒
     * @author Tank.Yang
     * @since 22019-05-29 00:35:19
     */
    @Override
    @SuppressWarnings("unchecked")
    public void set(final K k, final V v, final Long timeout, final TimeUnit unit) {
        ValueOperations vps = this.redisTemplate.opsForValue();
        // 是否設置超時時间
        if (null != timeout && timeout > 0L) {
            if (null == unit) {
                vps.set(k, v, timeout, TimeUnit.SECONDS);
            } else {
                vps.set(k, v, timeout, unit);
            }
        } else {
            vps.set(k, v);
        }
    }

    /**
     * <p>
     * 獲取緩存
     * </p>
     *
     * @param k 緩存Key
     * @return T
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    @Override
    public V get(final K k) {
        return this.redisTemplate.opsForValue().get(k);
    }

    /**
     * <p>
     * 是否存在Key
     * </p>
     *
     * @param k Key
     * @return boolean
     * @author Tank.Yang
     * @since 2019-10-18 15:15:09
     */
    @Override
    public boolean hasKey(final K k) {
        return this.redisTemplate.hasKey(k);
    }

    /**
     * <p>
     * 獲取多個Key列表
     * </p>
     *
     * @param keys Key列表
     * @return java.util.List<T>
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    @Override
    public List<V> multiGet(Set<K> keys) {
        return this.redisTemplate.opsForValue().multiGet(keys);
    }

    /**
     * <p>
     * 删除緩存
     * </p>
     *
     * @param k 緩存Key
     * @return boolean
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    @Override
    public boolean delete(final K k) {
        return Optional
                .ofNullable(this.redisTemplate.delete(k))
                .isPresent();
    }

    /**
     * <p>
     * 根据Key的前缀删除Key
     * </p>
     *
     * @param p Key前缀
     * @return boolean
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean deleteByPrefix(String p) {
        RedisTemplate redisTemplate = this.redisTemplate;
        Set<K> keys = redisTemplate.keys(p + "*");
        if (CollectionUtils.isEmpty(keys)) {
            return false;
        }
        return Optional
                .ofNullable(redisTemplate.delete(keys))
                .filter(d -> d.compareTo(0L) >= 1L)
                .isPresent();
    }

    /**
     * <p>
     * 给Map添加一個元素
     * </p>
     *
     * @param k k 緩存Key
     * @param f Map中的Key
     * @param v 值
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    @Override
    public void hSet(final K k, final String f, final V v) {
        this.redisTemplate.opsForHash().put(k, f, v);
    }

    /**
     * <p>
     * 緩存一個Map
     * </p>
     *
     * @param k k 緩存Key
     * @param m 整個Map
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    @Override
    public void hPutAll(final K k, final Map<K, V> m) {
        this.redisTemplate.opsForHash().putAll(k, m);
    }


    /**
     * <p>
     * 獲取Map中的某個元素
     * </p>
     *
     * @param k k 緩存Key
     * @param f Map中的Key
     * @return T
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    @Override
    @SuppressWarnings("unchecked")
    public V hGet(final K k, final String f) {
        return (V) this.redisTemplate.opsForHash().get(k, f);
    }


    /**
     * <p>
     * 獲取整個Map
     * </p>
     *
     * @param k 緩存Key
     * @return java.util.Map<java.lang.String, T>
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    @Override
    public Map entries(K k) {
        return this.redisTemplate.opsForHash().entries(k);
    }

    /**
     * <p>
     * 獲取Map内元素個數
     * </p>
     *
     * @param k Map Key
     * @return long
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    @Override
    public long hSize(final K k) {
        return this.redisTemplate.opsForHash().size(k);
    }


    /**
     * <p>
     * 獲取多個Key列表
     * </p>
     *
     * @param k    Map Key
     * @param keys Key列表
     * @return java.util.List<T>
     * @author Tank.Yang
     * @since 2019-05-29 00:35:19
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<V> multiGet(final K k, final List<K> keys) {
        return (List<V>) this.redisTemplate.opsForHash().multiGet(k, Collections.singleton(keys));
    }


    @Override
    public Set<K> keys(K pattern) {
        return this.redisTemplate.keys(pattern);
    }

}
