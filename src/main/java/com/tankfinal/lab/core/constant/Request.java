package com.tankfinal.lab.core.constant;

/**
 * <p> Request常數Key </p>
 *
 * @author Tank.Yang
 * @since 2020-05-29 15:51:30
 */
@SuppressWarnings("unused")
public interface Request {
    /**
     * <p> 請求頭 </p>
     *
     * @author Tank.Yang
     * @since 2020-05-29 15:51:39
     */
    interface Head {
        /**
         * 請求頭包含的内容
         */
        String MULTIPART = "multipart/";
        /**
         * 請求頭中的 IP
         */
        String X_FORWARDED_FOR = "x-forwarded-for";
        /**
         * 請求頭中的 IP
         */
        String WL_PROXY_CLIENT_IP = "WL-Proxy-Client-IP";
        /**
         * 請求頭中的 IP
         */
        String HTTP_CLIENT_IP = "HTTP_CLIENT_IP";
        /**
         * 請求頭中的 IP
         */
        String HTTP_X_FORWARDED_IP = "HTTP_X_FORWARDED_IP";
        /**
         * 請求頭中的 JWT
         */
        String JWT = "jwt";
        /**
         * 登陸者所屬用戶
         */
        String ORG_ROOT_ID = "orgRootId";
        /**
         * 登入者帳戶名稱
         */
        String USER_NAME = "username";
    }

    interface Default {
        /**
         * 預設使用者名稱
         */
        String DEFAULT_USER_NAME = "UNKNOWN";
        /**
         * 預設JWT認證內容
         */
        String DEFAULT_JWT_TOKEN = "UNKNOWN";
        /**
         * 預設登陸者所屬用戶
         */
        Integer DEFAULT_ORG_ROOT_ID = 1;
    }
}
