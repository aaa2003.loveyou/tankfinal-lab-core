package com.tankfinal.lab.core.constant;

/**
 * 系統通用參數
 *
 * @author Tank.Yang
 * @since 2021-12-23 13:20:01
 */
public interface SystemConstant {

    /**
     * Feign使用
     *
     * @author Tank.Yang
     * @since 2022-04-28 11:49:43
     */
    interface Feign {

        /**
         * 標頭使用
         *
         * @author Tank.Yang
         * @since 2022-04-28 11:50:50
         */
        interface Header {
            String FEIGN_TRACE_ID = "feignTraceId";
        }
    }
}
