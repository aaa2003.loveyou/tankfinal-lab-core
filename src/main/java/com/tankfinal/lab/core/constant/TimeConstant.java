package com.tankfinal.lab.core.constant;

/**
 * 時間相關常數
 *
 * @author Tank.Yang
 * @since 2020-10-13 10:30:09
 */
public interface TimeConstant {
    /**
     * 時間類型列舉(Quartz使用)
     *
     * @author Tank.Yang
     * @since 2020-10-13 10:31:29
     */
    enum IntervalUnitEnum {
        /**
         * 時間類型
         */
        NANOSECONDS,
        MILLISECONDS,
        SECONDS,
        MINUTES,
        HOURS,
        DAYS,
        WEEKS,
        MONTHS,
        YEARS;

        private IntervalUnitEnum() {
        }
    }
}
