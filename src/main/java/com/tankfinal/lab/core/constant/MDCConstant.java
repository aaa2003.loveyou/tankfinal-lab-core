package com.tankfinal.lab.core.constant;

/**
 * Logback客製化參數
 *
 * @author Tank.Yang
 * @since 2022-03-25 16:29:46
 */
public interface MDCConstant {
    String SESSION_ID = "sessionId";
    String REQUEST_URI = "requestURI";
    String USER_NAME = "username";
}
