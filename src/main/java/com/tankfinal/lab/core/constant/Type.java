package com.tankfinal.lab.core.constant;

/**
 * <p>
 * 缓存类型
 * </p>
 *
 * @author Tank.Yang
 * @since 2019-03-18 16:31:33
 */
@SuppressWarnings("unused")
public enum Type {

    /**
     * 缓存在Redis中的数据类型
     */
    DEFAULT,
    MAP,
    LIST,
    SET,
    ;

}
