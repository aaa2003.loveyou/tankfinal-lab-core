package com.tankfinal.lab.core.constant;

/**
 * 全局常量
 *
 * @author Tank.Yang
 * @since 2020-06-16 13:42:06
 */
public interface Global {
    /** 成功提示 */
    int SUCCESS = 0;
    /** 失敗提示 */
    int FAIL = -1;
    /** 系統異常提示消息 */
    String SYSTEM_ERROR = "系統發生異常";
    String SUCCESS_MSG = "成功";
    String UNKNOWN = "UNKNOWN";
    /**
     * 錯誤相關
     *
     * @author Tank.Yang
     * @since 2022-05-23 14:02:17
     */
    interface Error {
        /** 未知 */
        String UNKNOWN_ERROR = "UNKNOWN_ERROR";
        /** 未知的錯誤代碼 */
        String UNKNOWN_ERROR_CODE = "UNKNOWN_ERROR_CODE";
        /** 未知的語系 */
        String UNKNOWN_LOCALE = "UNKNOWN_LOCALE";
    }
    /**
     * 多國語系數據
     * @author Tank.Yang
     * @since 2020-09-09 18:26:28
     */
    interface MultiLocale {
        /** 語言檔位置 */
        String MESSAGE_PATH = "/message.yml";
        /** 錯誤訊息位置 */
        String ERROR = "error";
        /** Request Header名稱 */
        String LOCALE = "Locale";
        /** 錯誤訊息名稱 */
        String MESSAGE = "message";
        /** 錯誤訊息標題 */
        String TITLE = "title";
        /** HttpStatus錯誤碼 */
        Integer ERROR_CODE = 512;

        /**
         * 語言包
         * @author Tank.Yang
         * @since 2020-09-09 19:04:33
         */
        interface Language{
            String CN = "cn";
            String TW = "tw";
            String EN = "en";
        }
    }
    /**
     * 任務相關
     * @author Tank.Yang
     * @since 2020-09-22 17:12:54
     */
    interface Config{
        String TANKFINAL = "tankfinal";
        String CONFIG = ".config";
        /** JOB Runner 配置 */
        String JOB_PREFIX = TANKFINAL + CONFIG + ".job-runner";
        /** API 效能 監控配置 */
        String API_PERFORMANCE_MONITOR_PREFIX = TANKFINAL + CONFIG + ".api-performance";
    }
}
