package com.tankfinal.lab.core.constant;

/**
 * 緩存Key常量池(供應商:模組名:項目名:任務名:鎖)
 *
 * @author Tank.Yang
 * @since 2020-12-21 17:34:44
 */
public interface CacheKey {
    /**
     * KEY銜接符號
     */
    String COLON = ":";
    String TANKFINAL = "tankfinal";

    /**
     * Test模組緩存Key
     *
     * @author Tank.Yang
     * @since 2020-12-21 21:18:26
     */
    interface Lab1Sv {
        String MODULE_NAME = "tankfinal-lab1-service" + COLON;

        /**
         * 工廠使用CacheKey
         *
         * @author Tank.Yang
         * @since 2021-01-12 11:17:24
         */
        interface Factory {
            String FACTORY_NAME = CacheKey.TANKFINAL + Lab1Sv.MODULE_NAME + "factory" + COLON;
        }
    }
}
