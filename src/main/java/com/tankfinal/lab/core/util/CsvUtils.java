package com.tankfinal.lab.core.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.net.HttpHeaders;
import com.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * CSV處理用工具類
 *
 * @author will.huang
 * @since 2021-02-04 11:06:27
 */
@Slf4j
public class CsvUtils {

    /**
     * CSV檔案列分隔符
     */
    private static final String CSV_COLUMN_SEPARATOR = ",";

    /**
     * CSV檔案行分隔符
     */
    private static final String CSV_ROW_SEPARATOR = "\r\n";

    /**
     * 設置瀏覽器響應及調用Csv導出接口
     *
     * @param response
     * @param fileName 檔名
     * @param dataList 檔案內容
     * @author will.huang
     * @since 2021-01-21 19:45:03
     */
    public static void exportCsvInit(HttpServletResponse response, String fileName, List<Map<String, Object>> dataList,
                                     String titles, String keys)
            throws Exception {
        String fn = StringUtils.defaultIfBlank(fileName + ".csv", DateUtil.formatDate(new Date()) + ".csv");
        responseSetProperties(fn, response);
        doExport(dataList, titles, keys, response.getOutputStream());
    }

    public static void responseSetProperties(String fileName, HttpServletResponse response) throws UnsupportedEncodingException {
        // 設定檔案字尾
        fileName = URLEncoder.encode(fileName, "UTF-8");
        // 告訴瀏覽器用什麼軟件打開文件
        response.setHeader(HttpHeaders.CONTENT_TYPE, "application/vnd.ms-excel");
        // 下載文件的默認名稱
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;"
                + "filename=" + fileName
                + ";filename*=UTF-8''" + fileName);
        //允許操作獲取消息頭
        response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
    }

    /**
     * 導出csv資料內容到輸出流
     *
     * @param dataList 集合資料
     * @param titles   表頭部資料
     * @param keys     表內容的鍵值
     * @param os       輸出流
     * @return void
     * @author will.huang
     * @since 2021-02-04 11:08:26
     */
    private static void doExport(List<Map<String, Object>> dataList, String titles, String keys, OutputStream os) throws Exception {
        // 保證執行緒安全
        StringBuffer buf = new StringBuffer();

        String[] titleArr = titles.split(CSV_COLUMN_SEPARATOR);
        String[] keyArr = keys.split(CSV_COLUMN_SEPARATOR);

        // 組裝表頭
        for (String title : titleArr) {
            buf.append(title).append(CSV_COLUMN_SEPARATOR);
        }
        buf.append(CSV_ROW_SEPARATOR);
        // 組裝資料
        if (CollectionUtils.isNotEmpty(dataList)) {
            for (Map<String, Object> data : dataList) {
                for (String key : keyArr) {
                    buf.append(data.get(key)).append(CSV_COLUMN_SEPARATOR);
                }
                buf.append(CSV_ROW_SEPARATOR);
            }
        }
        // 寫出響應
        os.write(new byte[]{(byte) 0xEF, (byte) 0xBB, (byte) 0xBF});
        os.write(buf.toString().getBytes("UTF-8"));
        os.flush();
    }

    /**
     * 讀入csv資料的輸入流
     *
     * @param keys 表內容的鍵值
     * @param is   inputStream
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author will.huang
     * @since 2021-02-03 17:40:15
     */
    public static List<Map<String, Object>> doImport(String keys, InputStream is) throws Exception {
        List<Map<String, Object>> result = Lists.newArrayList();
        CSVReader reader = new CSVReader(new InputStreamReader(is, "UTF-8"));
        String[] record;
        String[] keyArr = keys.split(CSV_COLUMN_SEPARATOR);
        // 計算當前讀取行數
        int cnt = 0;
        while ((record = reader.readNext()) != null) {
            if (cnt++ == 0) {
                continue;
            }
            Map<String, Object> map = Maps.newHashMap();
            for (int i = 0; i < record.length; i++) {
                map.put(keyArr[i], record[i]);
            }
            result.add(map);
        }
        return result;
    }
}
