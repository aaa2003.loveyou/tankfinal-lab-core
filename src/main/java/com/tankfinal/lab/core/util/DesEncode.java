package com.tankfinal.lab.core.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import java.net.URLEncoder;
import java.security.Key;
import java.security.SecureRandom;
import java.util.Base64;
//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;


public class DesEncode {
    private transient final static Logger logger = LoggerFactory.getLogger(DesEncode.class);

    Key key;

    public DesEncode(String str) {
        setKey(str); //生成密匙
    }

    public DesEncode() {
        //此處不要修改，否則密鑰不一樣無法解密
        setKey("tankfinal");
    }

    /**
     * 根据参数生成KEY
     */
    public void setKey(String strKey) {

        try {
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");//修改后
            random.setSeed(strKey.getBytes());
            KeyGenerator _generator = KeyGenerator.getInstance("DES");
            _generator.init(random);
            this.key = _generator.generateKey();
            _generator = null;
        } catch (Exception e) {
            throw new RuntimeException(
                    "DesEncode setKey Error initializing SqlMap class. Cause: " + e);
        }
    }

    /**
     * 加密String明文输入,String密文输出
     */
    public String getEncString(String strMing) {
        byte[] byteMi = null;
        String strMi = "";
        //BASE64Encoder base64en = new BASE64Encoder();
        try {
            byteMi = this.getEncCode(strMing.getBytes("UTF-8"));
            //strMi = base64en.encode(byteMi);
            strMi = Base64.getEncoder().encodeToString(byteMi);
        } catch (Exception e) {
            throw new RuntimeException(
                    "DesEncode getEncString Error initializing SqlMap class. Cause: " + e);
        } finally {
            byteMi = null;
        }
        return strMi;
    }

    /**
     * 解密 以String密文输入,String明文输出
     *
     * @param strMi
     * @return
     */
    public String getDesString(String strMi) {
        //BASE64Decoder base64De = new BASE64Decoder();
        byte[] byteMing = null;
        byte[] byteMi = null;
        String strMing = "";
        try {
            //byteMi = base64De.decodeBuffer(strMi);

            byteMi = Base64.getDecoder().decode(strMi);
            byteMing = this.getDesCode(byteMi);
            strMing = new String(byteMing, "UTF-8");

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(
                    "DesEncode getDesString Error initializing SqlMap class. Cause: " + e);
        } finally {
            byteMing = null;
            byteMi = null;
        }
        return strMing;
    }

    /**
     * 加密以byte[]明文输入,byte[]密文输出
     *
     * @param byteS
     * @return
     */
    private byte[] getEncCode(byte[] byteS) {
        byte[] byteFina = null;
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byteFina = cipher.doFinal(byteS);
        } catch (Exception e) {
            throw new RuntimeException(
                    "DesEncode getEncCode Error initializing SqlMap class. Cause: " + e);
        } finally {
            cipher = null;
        }
        return byteFina;
    }

    /**
     * 解密以byte[]密文输入,以byte[]明文输出
     *
     * @param byteD
     * @return
     */
    private byte[] getDesCode(byte[] byteD) {
        Cipher cipher;
        byte[] byteFina = null;
        try {
            cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byteFina = cipher.doFinal(byteD);
        } catch (Exception e) {
            throw new RuntimeException(
                    "DesEncode getDesCode Error initializing SqlMap class. Cause: " + e);
        } finally {
            cipher = null;
        }
        return byteFina;
    }

    public static void main(String args[]) {
        DesEncode des = new DesEncode();


        String str1 = "$$tankfinal";
        //DES加密
        String str2 = des.getEncString(str1);
        String deStr = des.getDesString("MVn/MGKGGsU=");
        //String deStr = des.getDesString(str2);
        System.out.println("密文:" + str2);
        //DES解密
        System.out.println("明文:" + deStr);

        StringBuilder url = new StringBuilder();

        url.append("test");

        StringBuilder sb = new StringBuilder();
        sb.append("sdfdsf");
        String result = url.toString() + sb.toString();


        try {
            result = URLEncoder.encode("[", "UTF-8");
        } catch (Exception e) {
            // TODO: handle exception
        }
        System.out.println(result);
    }
}

