package com.tankfinal.lab.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class IpUtil {

    private static final Logger log = LoggerFactory.getLogger(IpUtil.class);

    public static String getIpAddr(HttpServletRequest request) {
        String ipAddress = null;
        try {
            ipAddress = request.getHeader("x-forwarded-for");

            log.info("x-forwarded-for ip:" + "[" + ipAddress + "]");

            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("Proxy-Client-IP");

                log.info("Proxy-Client-IP ip:" + "[" + ipAddress + "]");
            }

            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("WL-Proxy-Client-IP");

                log.info("WL-Proxy-Client-IP ip:" + "[" + ipAddress + "]");
            }

            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getRemoteAddr();

                log.info("remote addr ip:" + "[" + ipAddress + "]");

                if (ipAddress.equals("127.0.0.1")) {
                    // 根据网卡取本机配置的IP
                    InetAddress inet = null;
                    try {
                        inet = InetAddress.getLocalHost();
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
                    ipAddress = inet.getHostAddress();

                    log.info("host address ip:" + "[" + ipAddress + "]");
                }
            }
            // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
            if (ipAddress != null && ipAddress.length() > 15) { // "***.***.***.***".length()
                // = 15
                if (ipAddress.indexOf(",") > 0) {

                    log.info("before substring ip:" + "[" + ipAddress + "]");

                    ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));

                    log.info("substring ip:" + "[" + ipAddress + "]");
                }
            }
        } catch (Exception e) {
            ipAddress = "";
        }

        log.info("final ip:" + "[" + ipAddress + "]");

        return ipAddress;
    }
}
