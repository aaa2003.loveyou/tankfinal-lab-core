package com.tankfinal.lab.core.util;

import com.tankfinal.lab.core.exception.customize.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.support.CronSequenceGenerator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Cron工具類
 *
 * @author Tank.Yang
 * @since 2020-09-23 11:25:08
 */
@Slf4j
public class CronUtil {
    private final static String ASTERISK = "*";
    private final static String QUESTION_MARK = "?";
    private final static String ZERO = "0";
    private final static String FORMAT = "%s %s %s %s %s %s %s";

    /**
     * 根據傳入型態轉為Cron表達式
     *
     * @param obj 欲轉換的參數
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-09-23 11:25:52
     */
    public static String generateCronExpression(final Object obj) {
        String result = null;
        if (obj instanceof LocalTime) {
            LocalTime localTime = (LocalTime) obj;
            // EX:0 8 14 * * ? means At 14:08:00pm every day *表"每"意思 (cron表達式年為選填)
            result = String.format(FORMAT, ZERO, localTime.getMinute(), localTime.getHour(), ASTERISK, ASTERISK, QUESTION_MARK, "");
        } else if (obj instanceof LocalDate) {
            LocalDate localDate = (LocalDate) obj;
            result = String.format(FORMAT, ASTERISK, ASTERISK, ASTERISK, localDate.getDayOfMonth(), localDate.getMonth().getValue(), localDate.getDayOfWeek().getValue(), localDate.getYear());
        } else if (obj instanceof LocalDateTime) {
            LocalDateTime localDateTime = (LocalDateTime) obj;
            result = String.format(FORMAT, localDateTime.getSecond(), localDateTime.getMinute(), localDateTime.getHour(), localDateTime.getDayOfMonth(), localDateTime.getMonth().getValue(), localDateTime.getDayOfWeek().getValue(), localDateTime.getYear());
        }
        if (!CronSequenceGenerator.isValidExpression(result)) {
            throw new ServiceException("cron驗證失敗");
        }
        return result;
    }
}