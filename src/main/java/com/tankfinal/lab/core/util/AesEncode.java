package com.tankfinal.lab.core.util;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

public class AesEncode {
    //	Key secretKey;
    SecretKeySpec key;

    public AesEncode(String str) {
        setKey(str);  //生成密匙
    }

    public AesEncode() {
        //此處不要修改，否則密鑰不一樣無法解密
        setKey("tankfinal");
    }

    /**
     * 根据参数生成KEY
     */
    public void setKey(String strKey) {
        try {
            while (0 != (strKey.length() % 16)) {
                strKey = strKey + "_";
            }
            key = new SecretKeySpec(strKey.getBytes(), "AES");

//	        KeyGenerator _generator = KeyGenerator.getInstance("AES");
//	        java.security.SecureRandom random = java.security.SecureRandom.getInstance("SHA1PRNG"); 
//	        random.setSeed(strKey.getBytes());  
//	        _generator.init(128, random);
//	        SecretKey secretKey = _generator.generateKey();  
//	        byte[] enCodeFormat = secretKey.getEncoded();
//	        key = new SecretKeySpec(enCodeFormat, "AES"); 
//	        _generator = null;


        } catch (Exception e) {
            throw new RuntimeException(
                    "AesEncode setKey Error initializing SqlMap class. Cause: " + e);
        }
    }

    /**
     * 加密String明文输入,String密文输出
     */
    public String getEncString(String strMing) {
        byte[] byteMi = null;
        byte[] byteMing = null;
        String strMi = "";
        //BASE64Encoder base64en = new BASE64Encoder();
        Encoder base64en = java.util.Base64.getEncoder();
        try {
            byteMing = strMing.getBytes("UTF-8");
            byteMi = this.getEncCode(byteMing);
            //strMi = base64en.encode(byteMi);
            strMi = base64en.encodeToString(byteMi);
        } catch (Exception e) {
            throw new RuntimeException(
                    "AesEncode GetEncString Error initializing SqlMap class. Cause: " + e);
        } finally {
            base64en = null;
            byteMing = null;
            byteMi = null;
        }
        return strMi;
    }

    /**
     * 解密 以String密文输入,String明文输出
     *
     * @param strMi
     * @return
     */
    public String getDesString(String strMi) {
        //BASE64Decoder base64De = new BASE64Decoder();
        Decoder base64De = java.util.Base64.getDecoder();
        byte[] byteMing = null;
        byte[] byteMi = null;
        String strMing = "";
        try {
            //byteMi = base64De.decodeBuffer(strMi);
            byteMi = base64De.decode(strMi);
            byteMing = this.getDesCode(byteMi);
            strMing = new String(byteMing, "UTF8");

        } catch (Exception e) {
            throw new RuntimeException(
                    "Error initializing SqlMap class. Cause: " + e);
        } finally {
            base64De = null;
            byteMing = null;
            byteMi = null;
        }
        return strMing;
    }

    /**
     * 加密以byte[]明文输入,byte[]密文输出
     *
     * @param byteS
     * @return
     */
    private byte[] getEncCode(byte[] byteS) {
        byte[] byteFina = null;
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byteFina = cipher.doFinal(byteS);

        } catch (Exception e) {
            throw new RuntimeException(
                    "getEncCode Error initializing SqlMap class. Cause: " + e);
        } finally {
            cipher = null;
        }
        return byteFina;
    }

    /**
     * 解密以byte[]密文输入,以byte[]明文输出
     *
     * @param byteD
     * @return
     */
    private byte[] getDesCode(byte[] byteD) {
        Cipher cipher;
        byte[] byteFina = null;
        try {
            cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byteFina = cipher.doFinal(byteD);

        } catch (Exception e) {
            throw new RuntimeException(
                    "getDesCode Error initializing SqlMap class. Cause: " + e);
        } finally {
            cipher = null;
        }
        return byteFina;
    }

    public static void main(String args[]) {
        AesEncode des = new AesEncode();

        String str1 = "123456";
        //DES加密
        String str2 = des.getEncString(str1);
        String deStr = des.getDesString(str2);
        //String deStr = des.getDesString(str2);
        System.out.println("密文:" + str2);
        //DES解密
        System.out.println("明文:" + deStr);
    }
}

