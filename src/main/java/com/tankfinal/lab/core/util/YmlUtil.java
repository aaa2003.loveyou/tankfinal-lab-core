package com.tankfinal.lab.core.util;

import com.tankfinal.lab.core.exception.customize.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.PropertiesUtil;
import org.springframework.web.multipart.MultipartFile;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 讀取YAML配置文件
 *
 * @author Tank.Yang
 * @since 2020-06-16 13:43:33
 */
@Slf4j
public final class YmlUtil {

    private YmlUtil() {
    }

    /**
     * 獲取整個Yml中的配置信息
     *
     * @param path 文件名稱,如果要讀取的文件不在resources跟目錄下,則需要带上子路徑,如果文件在resources/mybatis目錄下,則path值為/mybatis/generator.yml
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author Tank.Yang
     * @since 2020-06-16 13:44:17
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> getYml(String path) {
        // check path  and check stream
        try (InputStream resourceAsStream = PropertiesUtil.class.getResourceAsStream(
                Optional.ofNullable(path)
                        .filter(StringUtils::isNotBlank)
                        .orElseThrow(() -> new ServiceException("profile path cannot be empty")))) {
            if (Objects.nonNull(resourceAsStream)) {
                return new Yaml().loadAs(resourceAsStream, Map.class);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    /**
     * 獲取整個Yml中的配置信息
     *
     * @param obj 欲轉換的類
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author Tank.Yang
     * @since 2020-06-16 13:44:17
     */
    @SuppressWarnings("unchecked")
    public static Object getYml(MultipartFile file, Object obj) {
        // check path  and check stream
        try (InputStream resourceAsStream = file.getInputStream()) {
            return new Yaml().loadAs(Optional.of(resourceAsStream).orElseThrow(() -> new ServiceException("yml profile is null")), obj.getClass());
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    /**
     * 根據Key獲取Yml中的配置訊息
     *
     * @param path 文件名稱,如果要讀取的文件不在resources跟目錄下,則需要带上子路徑,如果文件在resources/mybatis目錄下,則path值為/mybatis/generator.yml
     * @param key  配置Key
     * @return java.lang.Object
     * @author Tank.Yang
     * @since 2020-06-16 13:45:06
     */
    public static Object getYmlVal(String path, String key) {
        return Optional
                .ofNullable(getYml(path))
                .map(m -> m.get(
                        Optional.ofNullable(key)
                                .filter(StringUtils::isNotBlank)
                                .orElseThrow(() -> new ServiceException("key cannot be empty")))
                ).orElseThrow(() -> new ServiceException("key:" + key + " does not exist"));
    }
}
