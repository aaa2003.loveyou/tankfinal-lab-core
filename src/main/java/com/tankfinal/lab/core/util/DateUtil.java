package com.tankfinal.lab.core.util;

import com.tankfinal.lab.core.constant.TimeConstant;
import com.tankfinal.lab.core.exception.customize.SystemException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Slf4j
public class DateUtil {


    /**
     * 日期轉換為時分秒格式字符串<p>
     *
     * @throws ParseException
     * @author andy.wen
     * @date 2018年11月5日 下午3:27:50
     */
    public static String toStringSs(Date date) {
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.format(date);
        } else {
            return "";
        }
    }

    /**
     * 日期轉換為long<p>
     *
     * @throws ParseException
     * @author andy.wen
     * @date 2018年11月5日 下午3:27:50
     */
    public static long toLong(Date date) {
        return date != null ? date.getTime() : 0l;
    }

    /**
     * 轉換為十分秒格式日期<p>
     *
     * @author andy.wen
     * @date 2018年11月13日 上午11:27:02
     */
    public static Date toDateSs(String s) {
        SimpleDateFormat simpledateformat = null;
        if (StringUtil.trim(s).indexOf("-") >= 0) {
            simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        } else if (StringUtil.trim(s).indexOf("/") >= 0) {
            simpledateformat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        } else {
            throw new RuntimeException("date string is worng");
        }
        ParsePosition parseposition = new ParsePosition(0);
        Date date = simpledateformat.parse(s, parseposition);
        return date;
    }

    /**
     * 轉化當前日期為yyyyMMdd格式,以方便生成單號<p>
     *
     * @author eric.wang
     * @date 2019年5月11日 上午11:29:47
     */
    public static String toDateNo() {

        Date date = new Date();

        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

        String no = format.format(date);

        return no;
    }

    /**
     * 給指定日期往後推N天<p>
     *
     * @author eric.wang
     * @date 2019年5月21日 下午1:33:37
     */
    public static Date plusDay(Date date, int day) {

        Calendar ca = Calendar.getInstance();

        ca.setTime(date);

        ca.add(Calendar.DATE, day);

        return ca.getTime();
    }

    /**
     * 日期轉換為時分秒格式字符串(不含其他符號)(yyyyMMddHHmmssSSS)
     *
     * @param date 輸入時間
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-07-02 10:48:13
     */
    public static String getDateMilliSecond(Date date) {
        if (Objects.isNull(date)) {
            date = new Date();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return sdf.format(date);
    }

    /**
     * 日期格式化列舉
     *
     * @author Tank.Yang
     * @since 2020-08-03 11:52:16
     */
    public enum Formatter {
        /**
         * 日期格式化定義
         */
        DATE_PATTERN_SLASH("yyyy/MM/dd"),
        DATE_PATTERN_DASH("yyyy-MM-dd"),
        DATE_PATTERN_UNSIGNED("yyyyMMdd"),
        MONTH_PATTERN_SLASH("yyyy/MM"),
        MONTH_PATTERN_DASH("yyyy-MM"),
        MONTH_PATTERN_UNSIGNED("yyMM"),
        TIME_PATTERN("HH:mm:ss"),
        TIME_PATTERN_UNSIGNED("HHmmss"),
        TIME_PATTERN_WITHOUT_SECOND("HH:mm"),
        TIME_PATTERN_WITHOUT_SECOND_UNSIGNED("HHmm"),
        DATE_TIME_PATTERN_DASH("yyyy-MM-dd HH:mm:ss"),
        DATE_TIME_PATTERN("yyyy/MM/dd HH:mm:ss"),
        DATE_TIME_PATTERN_UNSIGNED("yyyyMMddHHmmss"),
        DATE_TIME_SSS_PATTERN("yyyy/MM/dd HH:mm:ss:SSS"),
        DATE_TIME_SSS_PATTERN_UNSIGNED("yyyyMMddHHmmssSSS"),
        ZONED_DATE_TIME_OFFSET("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"),
        ZONED_DATE_TIME_OFFSET2("yyyy-MM-dd'T'HH:mm:ss.SSS"),
        ZONED_DATE_TIME_OFFSET3("yyyy-MM-dd'T'HH:mm:ss"),
        ;

        /**
         * 格式
         */
        private final String pattern;

        Formatter(String pattern) {
            this.pattern = pattern;
        }

        public String getPattern() {
            return pattern;
        }

        public DateTimeFormatter getFormatter() {
            return DateTimeFormatter.ofPattern(this.pattern);
        }

        /**
         * 依據傳入的日期字串，取得適配的Formatter
         *
         * @param value 時間字串
         * @return com.tankfinal.core.util.DateUtil.Formatter
         * @author Tank.Yang
         * @since 2021-11-04 16:42:20
         */
        public static Formatter getByValue(String value) {
            return Arrays.stream(Formatter.values())
                    .filter(pattern -> {
                        try {
                            // 這個寫法很醜，未來需要重寫
                            if (value.contains(":")) {
                                LocalDateTime.parse(value, DateTimeFormatter.ofPattern(pattern.getPattern()));
                            } else {
                                LocalDate.parse(value, DateTimeFormatter.ofPattern(pattern.getPattern()));
                            }
                            return true;
                        } catch (Exception e) {
                            return false;
                        }
                    })
                    .findAny()
                    .orElse(null);
        }
    }

    private DateUtil() {
    }

    /**
     * <p>
     * 將字串轉成Date
     * </p>
     *
     * @param date 日期字串 yyyy/MM/dd
     * @return java.time.LocalDate
     * @author Tank
     * @since 2019-07-23 22:26:20
     */
    public static Date parseDate(String date) {
        if (StringUtils.isBlank(date)) {
            return null;
        }
        LocalDate localDate = LocalDate.parse(date, Formatter.DATE_PATTERN_SLASH.getFormatter());
        ZonedDateTime zdt = localDate.atStartOfDay(ZoneId.systemDefault());
        return Date.from(zdt.toInstant());
    }

    /**
     * <p>
     * 將字串轉成Date
     * </p>
     *
     * @param date      日期字串
     * @param formatter Formatter
     * @return java.time.LocalDate
     * @author Tank
     * @since 2021-04-02 10:43:00
     */
    public static Date parseDate(String date, Formatter formatter) {
        if (StringUtils.isBlank(date)) {
            return null;
        }
        LocalDateTime localDateTime;
        if (date.length() <= 10) {
            LocalDate localDate = LocalDate.parse(date, formatter.getFormatter());
            localDateTime = localDate.atStartOfDay();
        } else {
            localDateTime = LocalDateTime.parse(date, formatter.getFormatter());
        }
        ZonedDateTime zdt = localDateTime.atZone(ZoneId.systemDefault());
        return Date.from(zdt.toInstant());
    }

    /**
     * 將Date轉成字串
     *
     * @param date 日期格式
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-08-03 11:55:00
     */
    public static String formatDate(Date date) {
        if (Objects.isNull(date)) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(Formatter.DATE_TIME_PATTERN.pattern);
        return sdf.format(date);
    }

    /**
     * 將Date轉成字串Formatter
     *
     * @param date 要轉換的Date
     * @return String
     * @author paul.hsu
     * @since 2020-09-22 02:54:43
     */
    public static String formatDate(Date date, Formatter formatter) {
        if (Objects.isNull(date)) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(formatter.pattern);
        return sdf.format(date);
    }

    /**
     * LocalDate轉換成字串
     *
     * @param date 日期格式
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-08-03 11:55:36
     */
    public static String formatLocalDate(LocalDate date) {
        if (Objects.isNull(date)) {
            return null;
        }
        return date.format(Formatter.DATE_PATTERN_SLASH.getFormatter());
    }

    /**
     * LocalDate轉換成字串
     *
     * @param date      欲轉換的日期
     * @param formatter 格式
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-09-28 16:38:57
     */
    public static String formatLocalDate(LocalDate date, Formatter formatter) {
        if (Objects.isNull(date)) {
            return null;
        }
        return date.format(formatter.getFormatter());
    }

    /**
     * LocalDateTime轉換成字串
     *
     * @param date 日期格式
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-08-03 11:55:36
     */
    public static String formatLocalDateTime(LocalDateTime date) {
        if (Objects.isNull(date)) {
            return null;
        }
        return date.format(Formatter.DATE_PATTERN_SLASH.getFormatter());
    }

    /**
     * LocalDateTime根據指定format轉換成字串
     *
     * @param date 日期格式
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-08-03 11:55:36
     */
    public static String formatLocalDateTime(LocalDateTime date, Formatter formatter) {
        if (Objects.isNull(date)) {
            return null;
        }
        return date.format(formatter.getFormatter());
    }

    /**
     * 字串根據指定format轉換城LocalDateTime
     *
     * @param date 日期格式
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-08-03 11:55:36
     */
    public static LocalDateTime parseLocalDateTime(String date, Formatter formatter) {
        if (StringUtils.isBlank(date)) {
            return null;
        }
        return LocalDateTime.parse(date, formatter.getFormatter());
    }

    /**
     * 將Date轉換為LocalDateTime
     *
     * @param date 欲轉換的Date
     * @return java.time.LocalDateTime
     * @author Tank.Yang
     * @since 2020-10-22 16:21:09
     */
    public static LocalDateTime parseDate(Date date) {
        if (Objects.isNull(date)) {
            return null;
        }
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    /**
     * 將Date轉換為LocalDateTime(ZoneId)
     *
     * @param date       欲轉換的Date
     * @param zoneOffset ZoneId
     * @return java.time.LocalDateTime
     * @author Tank.Yang
     * @since 2021-08-11 14:07:53
     */
    public static LocalDateTime parseDateOfZone(Date date, ZoneOffset zoneOffset) {
        if (Objects.isNull(date)) {
            return null;
        }
        return date.toInstant().atZone(zoneOffset).toLocalDateTime();
    }

    /**
     * 將Date轉換為LocalTime
     *
     * @param date 欲轉換的Date
     * @return java.time.LocalDateTime
     * @author Tank.Yang
     * @since 2020-10-22 16:21:09
     */
    public static LocalTime parseDateToLocalTime(Date date) {
        if (Objects.isNull(date)) {
            return null;
        }
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
    }

    /**
     * 將Date轉換為LocalDate
     *
     * @param date 欲轉換的Date
     * @return java.time.LocalDateTime
     * @author Tank.Yang
     * @since 2020-10-22 16:21:09
     */
    public static LocalDate parseDateToLocalDate(Date date) {
        if (Objects.isNull(date)) {
            return null;
        }
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * 字串轉換成LocalDate（只接受yyyy/MM/DD）
     *
     * @param date 日期字串
     * @return java.time.LocalDate
     * @author Tank.Yang
     * @since 2020-08-03 11:58:38
     */
    public static LocalDate parseLocalDate(String date) {
        if (Objects.isNull(date)) {
            return null;
        }
        return LocalDate.parse(date, Formatter.DATE_PATTERN_SLASH.getFormatter());
    }

    /**
     * 字串轉換成LocalDate
     *
     * @param date 日期字串
     * @return java.time.LocalDate
     * @author Tank.Yang
     * @since 2020-08-03 11:58:38
     */
    public static LocalDate parseLocalDate(String date, Formatter formatter) {
        if (Objects.isNull(date)) {
            return null;
        }
        return LocalDate.parse(date, formatter.getFormatter());
    }

    /**
     * LocalDateTime轉為Date
     *
     * @param date 欲轉換的參數
     * @return java.util.Date
     * @author Tank.Yang
     * @since 2020-09-22 10:46:29
     */
    public static Date parseDate(LocalDateTime date) {
        if (Objects.isNull(date)) {
            return null;
        }
        return Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * LocalDateTime轉為Date(ZoneId)
     *
     * @param date       欲轉換的參數
     * @param zoneOffset ZoneOffset
     * @return java.util.Date
     * @author Tank.Yang
     * @since 2020-09-22 10:46:29
     */
    public static Date parseDateOfZone(LocalDateTime date, ZoneOffset zoneOffset) {
        if (Objects.isNull(date)) {
            return null;
        }
        return Date.from(date.atZone(zoneOffset).toInstant());
    }

    /**
     * 將LocalDateTime轉為long
     *
     * @param localDateTime 欲轉換的時間戳
     * @return long
     * @author Tank.Yang
     * @since 2020-10-23 11:54:50
     */
    public static Long localDateTimeToLong(LocalDateTime localDateTime) {
        if (Objects.isNull(localDateTime)) {
            return null;
        }
        return localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    /**
     * 將long轉為LocalDateTime
     *
     * @param dateLong 欲轉換的long
     * @return java.time.LocalDateTime
     * @author Tank.Yang
     * @since 2020-10-23 11:57:27
     */
    public static LocalDateTime longToLocalDateTime(Long dateLong) {
        if (Objects.isNull(dateLong)) {
            return null;
        }
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(dateLong), ZoneId.systemDefault());
    }

    /**
     * 將long轉為LocalDateTime(ZoneId)
     *
     * @param dateLong   欲轉換的long
     * @param zoneOffset ZoneOffset
     * @return java.time.LocalDateTime
     * @author Tank.Yang
     * @since 2020-10-23 11:57:27
     */
    public static LocalDateTime longToLocalDateTimeOfZone(Long dateLong, ZoneOffset zoneOffset) {
        if (Objects.isNull(dateLong)) {
            return null;
        }
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(dateLong), zoneOffset);
    }

    /**
     * <p>
     * 取得延遲後時間
     * 1 milliseconds = 1000000 nanoseconds
     * </p>
     *
     * @param delayUnit TimeUnit
     * @param delay     long
     * @return LocalDateTime
     * @author Tank
     * @since 2019-11-11 11:54:34
     */
    public static LocalDateTime getDelayedLocalDateTime(TimeConstant.IntervalUnitEnum delayUnit, long delay) {

        switch (delayUnit) {
            case NANOSECONDS:
                return LocalDateTime.now().plusNanos(delay);
            case MILLISECONDS:
                return LocalDateTime.now().plusNanos(delay * 1000000);
            case SECONDS:
                return LocalDateTime.now().plusSeconds(delay);
            case MINUTES:
                return LocalDateTime.now().plusMinutes(delay);
            case HOURS:
                return LocalDateTime.now().plusHours(delay);
            case DAYS:
                return LocalDateTime.now().plusDays(delay);
            case WEEKS:
                return LocalDateTime.now().plusWeeks(delay);
            case MONTHS:
                return LocalDateTime.now().plusMonths(delay);
            case YEARS:
                return LocalDateTime.now().plusYears(delay);
            default:
                throw new SystemException("發生未知錯誤");
        }
    }


    /**
     * 检核日期起迄
     *
     * @param startDay 日期起
     * @param endDay   日期迄
     * @author will.huang
     * @since 2020-09-17 14:41:59
     */
    public static boolean checkStartBeforeEnd(LocalDate startDay, LocalDate endDay) {
        return (Objects.isNull(startDay) || Objects.isNull(endDay)) || startDay.isAfter(endDay);
    }

    /**
     * 入日期回星期幾
     *
     * @param date 欲判斷日期
     * @author will.huang
     * @since 2020-09-18 17:36:18
     */
    public static Integer getDateToWeek(Date date) {
        if (Objects.isNull(date)) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        // 20200925 Will 因應前端判斷weekDay改動
        return calendar.get(Calendar.DAY_OF_WEEK) - 1;
    }

    /**
     * 補全給定起止時間區間內的所有日期
     *
     * @param startTime          開始時間
     * @param endTime            結束時間
     * @param isIncludeStartTime 日期是否包含開始時間
     * @param inputFormatter     輸入格式
     * @param outputFormatter    輸出格式
     * @return java.util.List<java.lang.String>
     * @author Tank.Yang
     * @since 2020-09-15 10:31:58
     */
    public static List<String> getBetweenDates(String startTime, String endTime, boolean isIncludeStartTime, Formatter inputFormatter, Formatter outputFormatter) {
        List<String> result = new ArrayList<>();
        try {
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputFormatter.getPattern());
            // 定義起始日期
            Date d1 = new SimpleDateFormat(inputFormatter.getPattern()).parse(startTime);
            // 定義結束日期
            Date d2 = new SimpleDateFormat(inputFormatter.getPattern()).parse(endTime);
            // 定義日期實例
            Calendar dd = Calendar.getInstance();
            // 設置起始時間
            dd.setTime(d1);
            if (isIncludeStartTime) {
                result.add(outputFormat.format(d1));
            }
            // 判斷是否到結束日期
            while (dd.getTime().before(d2)) {
                // 進行當前日期加一
                dd.add(Calendar.DATE, 1);
                String str = outputFormat.format(dd.getTime());
                result.add(str);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 補全給定起止時間區間內的所有日期
     *
     * @param startTime 開始時間
     * @param endTime   結束時間
     * @return java.util.List<java.time.LocalDate>
     * @author Tank.Yang
     * @since 2020-09-15 10:31:58
     */
    public static List<LocalDate> getBetweenDates(LocalDate startTime, LocalDate endTime) {
        List<LocalDate> result = new ArrayList<>();
        try {
            // 定義起始日期
            Date d1 = Date.from(startTime.atStartOfDay(ZoneId.systemDefault()).toInstant());
            // 定義結束日期
            Date d2 = Date.from(endTime.atStartOfDay(ZoneId.systemDefault()).toInstant());
            // 定義日期實例
            Calendar dd = Calendar.getInstance();
            // 設置起始時間
            dd.setTime(d1);
            result.add(startTime);
            // 判斷是否到結束日期
            while (dd.getTime().before(d2)) {
                // 進行當前日期加一
                dd.add(Calendar.DATE, 1);
                result.add(LocalDateTime.ofInstant(dd.toInstant(), dd.getTimeZone().toZoneId()).toLocalDate());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 補全給定起止時間區間內的所有日期
     *
     * @param startTime          開始時間
     * @param endTime            結束時間
     * @param isIncludeStartTime 日期是否包含開始時間
     * @return java.util.List<java.lang.String>
     * @author Tank.Yang
     * @since 2020-09-15 10:31:58
     */
    public static List<String> getBetweenDates(LocalDateTime startTime, LocalDateTime endTime, boolean isIncludeStartTime, Formatter outputFormatter) {
        List<String> result = new ArrayList<>();
        try {
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputFormatter.getPattern());
            // 定義起始日期
            Date d1 = parseDate(startTime);
            // 定義結束日期
            Date d2 = parseDate(endTime);
            // 定義日期實例
            Calendar dd = Calendar.getInstance();
            // 設置起始時間
            dd.setTime(d1);
            if (isIncludeStartTime) {
                result.add(outputFormat.format(d1));
            }
            // 判斷是否到結束日期
            while (dd.getTime().before(d2)) {
                // 進行當前日期加一
                dd.add(Calendar.DATE, 1);
                String str = outputFormat.format(dd.getTime());
                result.add(str);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 補全給定起止時間區間內的所有日期
     *
     * @param startTime       開始時間
     * @param endTime         結束時間
     * @param inputFormatter  輸入格式
     * @param outputFormatter 輸出格式
     * @return java.util.List<java.lang.String>
     * @author Tank.Yang
     * @since 2020-09-15 10:31:58
     */
    public static List<String> getMonthsBetweenDates(String startTime, String endTime, Formatter inputFormatter, Formatter outputFormatter) {
        List<String> result = new ArrayList<>();
        try {
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputFormatter.getPattern());
            // 定義起始月份
            Date d1 = new SimpleDateFormat(inputFormatter.getPattern()).parse(startTime);
            // 定義結束月份
            Date d2 = new SimpleDateFormat(inputFormatter.getPattern()).parse(endTime);
            // 定義日期實例
            Calendar dd = Calendar.getInstance();
            // 設置起始時間
            dd.setTime(d1);
            result.add(outputFormat.format(d1));
            // 判斷是否到结束日期
            while (dd.getTime().before(d2)) {
                // 進行當前日期月份加一
                dd.add(Calendar.MONTH, 1);
                String str = outputFormat.format(dd.getTime());
                result.add(str);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 補全給定起止時間區間內的所有週
     *
     * @param startTime      開始時間
     * @param endTime        結束時間
     * @param inputFormatter 輸入格式
     * @return java.util.List<java.lang.String>
     * @author Tank.Yang
     * @since 2020-09-15 10:31:58
     */
    public static ArrayList<String> getWeeksBetweenDates(String startTime, String endTime, Formatter inputFormatter) {
        ArrayList<String> result = new ArrayList<>();
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputFormatter.getPattern());
        try {
            // 定義起始時間
            Date start = inputFormat.parse(startTime);
            // 定義結束時間
            Date end = inputFormat.parse(endTime);
            Calendar calendar = Calendar.getInstance();
            // 定義周日為一週的開始
            calendar.setFirstDayOfWeek(Calendar.SUNDAY);
            calendar.setTime(start);
            // 加入當前週
            result.add(String.valueOf(calendar.get(Calendar.WEEK_OF_YEAR)));
            // 判斷是否到結束的月份
            while (calendar.getTime().before(end)) {
                // 進行當前日期週加一
                calendar.add(Calendar.WEEK_OF_YEAR, 1);
                result.add(String.valueOf(calendar.get(Calendar.WEEK_OF_YEAR)));
            }
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 根據YYmm格式獲取當前日期
     *
     * @Return: java.lang.String
     * @Author allen.lee
     * @Since 2021/6/22 17:10:53
     */
    public static String getNowDateYYmm() {

        LocalDate localDate = LocalDate.now();
        return localDate.format(DateTimeFormatter.ofPattern(Formatter.MONTH_PATTERN_UNSIGNED.pattern));
    }


    /**
     * 將傳入的日期字串，依據格式化器挑選最終轉成LocalDateTime
     *
     * @param dateStr 請輸入用途
     * @return java.time.LocalDateTime
     * @author Tank.Yang
     * @since 2021-11-04 16:35:24
     */
    public static LocalDateTime parseLocalDateTimeByFormatter(String dateStr) {
        if (StringUtils.isBlank(dateStr)) {
            return null;
        }
        return DateUtil.parseLocalDateTime(dateStr, Formatter.getByValue(dateStr));
    }

    /**
     * 字串轉換成LocalDateTime，接受任何格式的字串，若不為Formatter的成員，則為空
     *
     * @param dateStr 請輸入用途
     * @return java.time.LocalDateTime
     * @author Tank.Yang
     * @since 2021-11-04 16:35:24
     */
    public static LocalDateTime parseLocalDateTime(String dateStr) {
        if (dateStr == null || dateStr.trim().length() == 0) {
            return null;
        }
        Formatter formatter = Formatter.getByValue(dateStr);
        if (formatter != null) {
            // 若傳送TimeZone時區，則轉換為系統時區
            if (formatter.equals(Formatter.ZONED_DATE_TIME_OFFSET)) {
                return ZonedDateTime.parse(dateStr)
                        .withZoneSameInstant(ZoneId.systemDefault())
                        .toLocalDateTime();
            } else {
                return DateUtil.parseLocalDateTime(dateStr, formatter);
            }
        }
        return null;
    }

    /**
     * 字串轉換成LocalDateTime，接受任何格式的字串，若不為Formatter的成員，則為空
     *
     * @param dateStr 請輸入用途
     * @return java.time.LocalDateTime
     * @author Tank.Yang
     * @since 2021-11-04 16:35:24
     */
    public static ZonedDateTime parseZonedDateTimeWithFormatter(String dateStr) {
        if (dateStr == null || dateStr.trim().length() == 0) {
            return null;
        }
        Formatter formatter = Formatter.getByValue(dateStr);
        if (formatter != null) {
            // 若傳送TimeZone時區，則轉換為系統時區
            if (formatter.equals(Formatter.ZONED_DATE_TIME_OFFSET)) {
                return ZonedDateTime.parse(dateStr);
            } else {
                // 這個寫法很醜，未來需要重寫
                try {
                    LocalDateTime localDateTime = DateUtil.parseLocalDateTime(dateStr, formatter);
                    return localDateTime.atZone(ZoneId.systemDefault());
                } catch (Exception e) {
                    try {
                        LocalDateTime localDateTime = LocalDateTime.of(DateUtil.parseLocalDate(dateStr, formatter), LocalTime.MIN);
                        return localDateTime.atZone(ZoneId.systemDefault());
                    } catch (Exception e1) {
                        log.error("不支援的時區格式");
                    }
                }

            }
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println("formatter=" + parseZonedDateTimeWithFormatter("2022-01-22"));
    }

}
