package com.tankfinal.lab.core.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class I18nUtil {

    public static <E> String getI18n(Class<E> enumType, String columnName, String status) throws Exception {
        String result = "UNKNOW";
        //取得列舉values的方法
        Method methodOfValues = enumType.getDeclaredMethod("values", (Class[]) null);
        Object values = methodOfValues.invoke(null, (Object[]) null);
        E[] array = (E[]) values;
        for (E e : array) {
            String camelName = StringUtil.toCamelCase(e.toString());
            camelName = StringUtil.toHeadLowerCase(camelName);
            if (camelName.indexOf(columnName) != -1) {
                Method methodOfValue = ((Class<E>) e).getDeclaredMethod("value", (Class[]) null);
                String enumValue = (String) methodOfValue.invoke(null, (String) null);
                if (enumValue.equals(status)) {
                    Field field = ((Class<E>) e).getDeclaredField("i18n");
                    field.setAccessible(true);
                    return result = (String) field.get(e);
                }
            }
        }
        return result;
    }
}
