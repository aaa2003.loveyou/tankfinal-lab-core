package com.tankfinal.lab.core.util;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 分頁相關util類
 *
 * @author lin.chen
 * @since 2020-06-11 16:13
 */
public class PageUtil {

    // 初始頁數
    public static int startPage = 0;
    // 每頁最大筆數
    public static int maxSizePerPage = 50;

    /**
     * 傳入全部list，回傳分頁結果list
     *
     * @param list     全部list資料
     * @param pageNum  開始頁數為0
     * @param pageSize 每頁多少筆資料
     * @return java.util.List<java.lang.Object>
     * @author lin.chen
     * @since 2020-06-11 16:14
     */
    public static <T> List<T> startPage(List<T> list, Integer pageNum, Integer pageSize) {
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }

        if (pageNum < 0 || pageSize <= 0) {
            return new ArrayList<>();
        }

        Integer count = list.size(); // 紀錄總數
        Integer pageCount = 0; // 頁數

        if (count < pageSize) {
            pageSize = count;
        }
        if (count % pageSize == 0) {
            pageCount = count / pageSize;
        } else {
            pageCount = count / pageSize + 1;
        }

        int fromIndex = 0; // 開始索引
        int toIndex = 0; // 結束索引

        if ((pageNum.intValue() + 1) != pageCount.intValue()) {
            fromIndex = pageNum * pageSize;
            toIndex = fromIndex + pageSize;
        } else {
            fromIndex = pageNum * pageSize;
            toIndex = count;
        }

        return list.subList(fromIndex, toIndex);

    }
}
