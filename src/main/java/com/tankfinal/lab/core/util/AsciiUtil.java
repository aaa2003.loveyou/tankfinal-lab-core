package com.tankfinal.lab.core.util;

/**
 * ASCII 轉換工具
 *
 * @author charles.chen
 * @date 2018年8月24日 下午3:27:20
 */
public class AsciiUtil {

    /**
     * 取得字串第一個字母轉成ASCII碼
     *
     * @author charles.chen
     * @date 2018年8月24日 下午3:29:11
     */
    public static int toAscii(String str) {
        char strChar = str.charAt(0);
        //字母轉成ascii碼
        int ascii = (int) strChar;
        return ascii;
    }

    /**
     * 將ascii碼轉為字母
     *
     * @author charles.chen
     * @date 2018年8月24日 下午3:30:32
     */
    public static String toLetter(int ascii) {
        String letter = Character.toString((char) ascii);
        return letter;
    }
}
