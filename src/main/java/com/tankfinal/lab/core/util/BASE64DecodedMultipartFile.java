package com.tankfinal.lab.core.util;

import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

/**
 * Base64轉multipart file格式工具類
 *
 * @author will.huang
 * @since 2021-12-21 11:24:16
 */
public class BASE64DecodedMultipartFile implements MultipartFile {

    private final byte[] imgContent;
    private final String[] tmpBase64;

    public BASE64DecodedMultipartFile(byte[] imgContent, String[] tmpBase64) {
        this.imgContent = imgContent;
        // base64String | originalFileName | size bytes
        this.tmpBase64 = tmpBase64;

    }

    @Override
    public String getName() {
        return tmpBase64[1];
    }

    @Override
    public String getOriginalFilename() {
        return tmpBase64[1];
    }

    @Override
    public String getContentType() {
        // data:image/jpeg;base64 , base64String
        return MediaType.MULTIPART_FORM_DATA_VALUE;
    }

    @Override
    public boolean isEmpty() {
        return imgContent == null || imgContent.length == 0;
    }

    @Override
    public long getSize() {
        return Long.getLong(tmpBase64[2]);
    }

    @Override
    public byte[] getBytes() throws IOException {
        return imgContent;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(imgContent);
    }

    @Override
    public void transferTo(File dest) throws IOException, IllegalStateException {
        new FileOutputStream(dest).write(imgContent);
    }
}
