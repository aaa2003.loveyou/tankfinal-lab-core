package com.tankfinal.lab.core.util;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 處理MAP通過key非大小寫限制獲取值等<p>
 * <p>
 * <strong>className類初使化方法</strong>
 * <ul>
 * <li>方法1.
 * <li>方法2.
 * </ul>
 *
 * @author eric.wang
 * @version 1.0, 2019年1月30日
 * @since JDK1.8
 */

public class MapUtil {

    /**
     * 通過非大小寫區分的kay獲取值<p>
     *
     * @author eric.wang
     * @date 2019年1月30日 下午8:12:12
     */
    public static String getValue(String key, Map<String, String> map) {
        if (StringUtils.isBlank(key) || MapUtils.isEmpty(map)) {
            return null;
        } else {
            String result = null;
            Set<Entry<String, String>> set = map.entrySet();
            for (Entry<String, String> entry : set) {
                if (entry.getKey().toUpperCase().equals(key.toUpperCase())) {
                    result = entry.getValue();
                    break;
                }
            }
            return result;
        }
    }

    /**
     * 依據Key做排序
     *
     * @param map 欲排序的Map
     * @return java.util.Map<K, V>
     * @author Tank.Yang
     * @since 2020-09-22 16:14:24
     */
    public static <K extends Comparable<? super K>, V> Map<K, V> sortByKey(Map<K, V> map) {
        Map<K, V> result = new LinkedHashMap<>();
        map.entrySet().stream()
                .sorted(Map.Entry.<K, V>comparingByKey()
                        .reversed()).forEachOrdered(e -> result.put(e.getKey(), e.getValue()));
        return result;
    }

    /**
     * 依據Value做排序
     *
     * @param map 欲排序的Map
     * @return java.util.Map<K, V>
     * @author Tank.Yang
     * @since 2020-09-22 16:14:24
     */
    public <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        Map<K, V> result = new LinkedHashMap<>();
        map.entrySet().stream()
                .sorted(Map.Entry.<K, V>comparingByValue()
                        .reversed()).forEachOrdered(e -> result.put(e.getKey(), e.getValue()));
        return result;
    }
}
