package com.tankfinal.lab.core.util;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Calendar工具
 *
 * @author paul.hsu
 * @since 2020-09-18 11:21:28
 */
@Slf4j
public final class CalendarUtil {
    /**
     * 一天的總時數
     */
    public final static Long ONE_DAY_TIME_IN_MILLIS = 86400000L;

    /**
     * 將LocalDateTime轉為日曆格式
     *
     * @param localDateTime 欲轉換的時間
     * @return java.util.Calendar
     * @author Tank.Yang
     * @since 2020-09-18 21:00:42
     */
    public static Calendar parse(LocalDateTime localDateTime) {
        Calendar result = Calendar.getInstance();
        result.set(localDateTime.getYear(), (localDateTime.getMonthValue() - 1), localDateTime.getDayOfMonth(),
                localDateTime.getHour(), localDateTime.getMinute(), 0);
        result.set(Calendar.MILLISECOND, 0);
        return result;
    }

    /**
     * 將timestamp轉為日曆格式
     *
     * @param timeStamp 要轉換的timestamp
     * @return java.util.Calendar
     * @author Tank.Yang
     * @since 2020-09-18 21:00:05
     */
    public static Calendar parse(Long timeStamp) {
        Calendar result = Calendar.getInstance();
        result.setTime(new Date(timeStamp));
        return result;
    }

    /**
     * 取得當天0:00時間
     *
     * @param localDateTime 欲改動的時間
     * @return java.util.Calendar
     * @author Tank.Yang
     * @since 2020-09-18 20:58:01
     */
    public static Calendar getTheDayFirstTime(LocalDateTime localDateTime) {
        Calendar result = Calendar.getInstance();
        result.set(localDateTime.getYear(), (localDateTime.getMonthValue() - 1),
                localDateTime.getDayOfMonth(), 0, 0,
                0);
        result.set(Calendar.MILLISECOND, 0);
        return result;
    }

    /**
     * 比較是否同天
     *
     * @param start 日期1
     * @param end   日期2
     * @return boolean 是否同天
     * @author paul.hsu
     * @since 2020-09-18 04:48:56
     */
    public static boolean isTheSameDay(Calendar start, Calendar end) {
        boolean result = (start.get(Calendar.YEAR) == end.get(Calendar.YEAR) && start.get(Calendar.MONTH) == end.get(Calendar.MONTH)
                && start.get(Calendar.DATE) == end.get(Calendar.DATE));
        if (!result) {
            result = (ONE_DAY_TIME_IN_MILLIS >= (+(end.getTimeInMillis() - start.getTimeInMillis()))
                    && end.get(Calendar.HOUR) == 0L && end.get(Calendar.MINUTE) == 0L && end.get(Calendar.SECOND) == 0L);
        }
        return result;
    }

    /**
     * 取得隔日的00分
     *
     * @param localDateTime 欲取得的時間
     * @return java.util.Calendar
     * @author Tank.Yang
     * @since 2020-09-18 21:01:55
     */
    public static Calendar getNextDayFirstTime(LocalDateTime localDateTime) {
        Calendar result = Calendar.getInstance();
        result.set(localDateTime.getYear(), (localDateTime.getMonthValue() - 1),
                localDateTime.getDayOfMonth() + 1, 0, 0,
                0);
        result.set(Calendar.MILLISECOND, 0);
        return result;
    }

    /**
     * 根據 年、月 獲取週數
     *
     * @param year
     * @param month
     * @return
     * @author paul.hsu
     * @since 2020-09-21 02:51:45
     */
    public static Set<Integer> getWeekOfYear(Integer year, Integer month) {

        Set<Integer> result = new HashSet<>();
        //**第一天
        int i = 1;
        Calendar start = Calendar.getInstance();

        while (true) {

            start.set(year, (month - 1), i, 0, 0, 0);
            i++;
            if (month - 1 != start.get(Calendar.MONTH)) {
                break;
            }
            result.add(start.get(Calendar.WEEK_OF_YEAR));
        }
        return result;
    }

    /**
     * 根據calendar，獲取時間自串，
     *
     * @param calendar
     * @return
     * @author paul.hsu
     * @since 2020-10-23 10:27:39
     */
    public static String getDateString(Calendar calendar) {

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        LocalDateTime localDateTime = LocalDateTime.of(year, month, day, 0, 0);
        return DateUtil.formatLocalDateTime(localDateTime, DateUtil.Formatter.DATE_PATTERN_DASH);
    }

    /**
     * 根據起、訖時間，獲取日期字串 yyyy-mm-dd
     *
     * @param startTime
     * @param endTime
     * @return
     * @author paul.hsu
     * @since 2020-10-23 09:32:32
     */
    public static Set<String> getDateString(LocalDateTime startTime, LocalDateTime endTime) {

        Set<String> result = new HashSet<>();
        if (startTime != null && endTime != null && startTime.isBefore(endTime)) {

            while (true) {
                result.add(DateUtil.formatLocalDateTime(startTime, DateUtil.Formatter.DATE_PATTERN_DASH));
                if (startTime.isBefore(endTime) == false) {
                    break;
                }
                startTime = startTime.plusDays(1L);
            }
        }
        return result;
    }

}
