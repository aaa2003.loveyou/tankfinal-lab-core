package com.tankfinal.lab.core.framework;

import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.EurekaClientConfig;
import com.netflix.discovery.shared.Application;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * <p>
 * Spring上下文容器工具類
 * </p>
 *
 * @author Tank.Yang
 * @since 2019-12-20 16:51:59
 */
@Slf4j
@Component
public final class SpringUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    /**
     * <p>
     * 初始化注入上下文實例
     * </p>
     *
     * @param applicationContext 上下文对象
     * @author Tank.Yang
     * @since 2019-07-30 16:01:33
     */
    @Override
    public void setApplicationContext(@SuppressWarnings("NullableProblems") ApplicationContext applicationContext) throws BeansException {
        log.info(">>>>>>>> 裝載 ApplicationContext 上下文對象 <<<<<<<<");
        SpringUtil.applicationContext = applicationContext;
    }

    /**
     * <p>
     * 根據Bean名稱獲取Bean實例
     * 適用於目標Bean只存在一種的情况下
     * </p>
     *
     * @param name Bean名稱
     * @return java.lang.Object
     * @author Tank.Yang
     * @date 2019-03-10 22:44:59
     */
    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    /**
     * <p>
     * 根據Bean類型獲取Bean實例
     * 適用於目標Bean只存在一種的情况下
     * </p>
     *
     * @param tClass 目標類型
     * @return T
     * @author Tank.Yang
     * @date 2019-03-10 22:44:30
     */
    public static <T> T getBean(Class<T> tClass) {
        return applicationContext.getBean(tClass);
    }

    /**
     * <p>
     * 從容器中獲取Bean實例
     * </p>
     *
     * @param name         Bean名稱
     * @param requiredType Bean類型
     * @return T
     * @author Tank.Yang
     * @date 2019-03-10 22:44:08
     */
    public static <T> T getBean(String name, Class<T> requiredType) {
        return applicationContext.getBean(name, requiredType);
    }

    /**
     * <p>
     * 上下文中是否存在該Bean
     * </p>
     *
     * @param name Bean名稱
     * @return boolean
     * @author Tank.Yang
     * @date 2019-03-10 22:43:47
     */
    public static boolean containsBean(String name) {
        return applicationContext.containsBean(name);
    }

    /**
     * <p>
     * 該Bean是否是單例
     * </p>
     *
     * @param name Bean名稱
     * @return boolean
     * @author Tank.Yang
     * @date 2019-03-10 22:43:19
     */
    public static boolean isSingleton(String name) {
        return applicationContext.isSingleton(name);
    }

    /**
     * <p>
     * 根據Bean名稱獲取Bean類型
     * </p>
     *
     * @param name Bean名稱
     * @return java.lang.Class<? extends java.lang.Object>
     * @author Tank.Yang
     * @date 2019-03-10 22:45:26
     */
    public static Class<?> getType(String name) {
        return applicationContext.getType(name);
    }

    /**
     * <p>
     * 通過requiredType類型獲取他的所有子類型實例
     * </p>
     *
     * @param requiredType 目標類型
     * @return java.util.Map<java.lang.String, T>
     * @author Lyn
     * @since 2020-01-09 14:57:35
     */
    public static <T> Map<String, T> getBeans(Class<T> requiredType) {
        return applicationContext.getBeansOfType(requiredType);
    }

    /**
     * 指定服務名稱是否存在
     *
     * @param serviceName 服務名稱
     * @return java.lang.Boolean
     * @author Tank.Yang
     * @since 2021-08-25 16:30:23
     */
    public static Boolean hasService(String serviceName) {
        EurekaClient client = getBean(EurekaClient.class);
        Application application = client.getApplication(serviceName);
        return application != null;
    }

    /**
     * 取得服務
     *
     * @param serviceName 服務名稱
     * @return com.netflix.discovery.shared.Application
     * @author Tank.Yang
     * @since 2021-08-25 16:30:59
     */
    public static Application getService(String serviceName) {
        EurekaClient client = getBean(EurekaClient.class);
        return client.getApplication(serviceName);
    }

    /**
     * 取得Eureka客戶端配置
     *
     * @return com.netflix.discovery.EurekaClientConfig
     * @author Tank.Yang
     * @since 2021-08-25 16:33:44
     */
    public static EurekaClientConfig getEurekaClientConfig() {
        EurekaClient client = getBean(EurekaClient.class);
        return client.getEurekaClientConfig();
    }
}
