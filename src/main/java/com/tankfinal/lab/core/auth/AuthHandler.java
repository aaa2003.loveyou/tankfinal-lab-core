package com.tankfinal.lab.core.auth;

import com.tankfinal.lab.core.constant.Request;
import com.tankfinal.lab.core.util.StringUtil;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Optional;

/**
 * 全局認證類
 *
 * @author Tank.Yang
 * @since 2020-11-05 11:26:57
 */
public class AuthHandler {
    /**
     * 取得已登入Header中的username
     * <p>若本身不具有HttpServletRequest，也會給UNKNOWN</p>
     *
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-11-05 11:22:43
     */
    public static String getUsername() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (Objects.nonNull(requestAttributes)) {
            return Optional.ofNullable(((ServletRequestAttributes) requestAttributes)
                            .getRequest()
                            .getHeader(Request.Head.USER_NAME))
                    .orElse(Request.Default.DEFAULT_USER_NAME);
        }
        return Request.Default.DEFAULT_USER_NAME;
    }

    /**
     * 獲得登陸者所屬用戶
     *
     * @return java.lang.Integer
     * @author Tank.Yang
     * @since 2020-11-05 11:52:01
     */
    public static Integer getUserOrgRootId() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (Objects.nonNull(requestAttributes)) {
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            String header = request.getHeader(Request.Head.ORG_ROOT_ID);
            if (StringUtil.isNumber(header)) {
                return Integer.parseInt(header);
            }
        }
        return Request.Default.DEFAULT_ORG_ROOT_ID;
    }


    /**
     * 取得已登入Header中的token資訊
     *
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-11-05 11:51:46
     */
    public static String getToken() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (Objects.nonNull(requestAttributes)) {
            return Optional.ofNullable(((ServletRequestAttributes) requestAttributes)
                            .getRequest()
                            .getHeader(Request.Head.JWT))
                    .orElse(Request.Default.DEFAULT_JWT_TOKEN);
        }
        return Request.Default.DEFAULT_JWT_TOKEN;
    }
}
