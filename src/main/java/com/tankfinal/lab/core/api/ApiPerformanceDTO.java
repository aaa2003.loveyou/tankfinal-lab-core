package com.tankfinal.lab.core.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * API效能分析DTO
 *
 * @author Tank.Yang
 * @since 2020-11-20 15:07:27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ApiPerformanceDTO {
    /**
     * API請求起始時間
     */
    Long timeS;
    /**
     * API請求結束時間
     */
    Long timeE;
    /**
     * API請求URL
     */
    String url;
    /**
     * 經過時間
     */
    Long duration;
    /**
     * 時間單位
     */
    String timeUnit;
}
