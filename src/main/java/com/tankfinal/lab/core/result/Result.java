package com.tankfinal.lab.core.result;

import com.tankfinal.lab.core.result.base.ResultSuper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * 自定義返回結果
 *
 * @author Tank.Yang
 * @since 2020-06-24 14:26:14
 */
@Getter
@Setter
@ApiModel(description = "回傳容器(夾帶數據時使用)")
@NoArgsConstructor
public class Result<T> extends ResultSuper {

    private static final long serialVersionUID = -8352295011141623129L;

    public static final String DATA = "data";

    public static final String URL = "url";

    public static final String DATE = "date";

    public static final String TITLE = "title";

    /**
     * 請求的資源路徑
     */
    @ApiModelProperty(value = "請求的資源路徑", required = false)
    private String url;

    /**
     * 當下時間
     */
    @ApiModelProperty(value = "當下時間", required = false)
    private Date date;

    /**
     * 錯誤內容
     */
    @ApiModelProperty(value = "錯誤內容", required = false)
    private String detail;

    /**
     * 返回内容
     */
    @ApiModelProperty(value = "回傳數據內容", required = false)
    private T data;

    @Builder
    public Result(int code, String message, Date date, String url, String detail, T data) {
        super(code, message);
        this.date = date;
        this.url = url;
        this.detail = detail;
        this.data = data;
    }
}
