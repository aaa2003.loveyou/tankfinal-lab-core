package com.tankfinal.lab.core.result.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * 返回結果基本類
 *
 * @author Tank.Yang
 * @since 2020-06-24 14:25:55
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "回傳容器(一般回傳時使用)")
public class ResultSuper implements Serializable {

    private static final long serialVersionUID = -8352295011141623129L;

    public static final String CODE = "code";
    public static final String MESSAGE = "message";

    /**
     * 狀態碼
     */
    @ApiModelProperty(value = "狀態碼", required = false)
    private int code;
    /**
     * 消息
     */
    @ApiModelProperty(value = "消息", required = false)
    private String message;
}
