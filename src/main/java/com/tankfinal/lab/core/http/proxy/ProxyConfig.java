package com.tankfinal.lab.core.http.proxy;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 代理配置
 *
 * @author Tank.Yang
 * @since 2020-06-18 11:14:01
 */
@Getter
@Setter
@Builder
public class ProxyConfig {

    /**
     * 代理主機
     */
    private String host;
    /**
     * 代理端口
     */
    private int port;
}
