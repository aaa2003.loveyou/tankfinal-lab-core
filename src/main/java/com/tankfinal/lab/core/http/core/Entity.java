package com.tankfinal.lab.core.http.core;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * HTTP Core Entity
 *
 * @author Tank.Yang
 * @since 2020-06-18 11:14:16
 */
@Getter
@Setter
@Builder
public class Entity {

    /**
     * Http 狀態碼
     */
    private int statusCode;
    /**
     * Http 返回结果
     */
    private String result;

    @Override
    public String toString() {
        return "Entity{" +
                "statusCode=" + statusCode +
                ", result='" + result + '\'' +
                '}';
    }
}
