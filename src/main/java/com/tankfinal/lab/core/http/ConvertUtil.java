package com.tankfinal.lab.core.http;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 轉換器通用類
 *
 * @author Tank.Yang
 * @since 2020-06-19 16:23:12
 */
public class ConvertUtil {

    /**
     * 將Map轉換為Object
     *
     * @param map       欲轉換的map
     * @param beanClass Object類型
     * @return java.lang.Object
     * @author Tank.Yang
     * @since 2020-06-19 16:26:13
     */
    public static Object mapToObject(Map<String, Object> map, Class<?> beanClass) {
        if (map == null) {
            return null;
        }
        Object obj = null;
        try {
            obj = beanClass.newInstance();
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                Method setter = property.getWriteMethod();
                if (setter != null) {
                    setter.invoke(obj, map.get(property.getName()));
                }
            }
        } catch (IntrospectionException | IllegalAccessException | InvocationTargetException |
                 InstantiationException e) {
            return null;
        }
        return obj;
    }

    /**
     * 將Objec轉成Map
     *
     * @param obj 欲轉換的obj
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author Tank.Yang
     * @since 2020-06-19 16:26:57
     */
    public static Map<String, Object> objectToMap(Object obj) {
        if (obj == null) {
            return null;
        }

        Map<String, Object> map = new HashMap<String, Object>();

        BeanInfo beanInfo = null;
        try {
            beanInfo = Introspector.getBeanInfo(obj.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();
                if (key.compareToIgnoreCase("class") == 0) {
                    continue;
                }
                Method getter = property.getReadMethod();
                Object value = getter != null ? getter.invoke(obj) : null;
                map.put(key, value);
            }
        } catch (IntrospectionException | IllegalAccessException | InvocationTargetException e) {
            return null;
        }
        return map;
    }

}
