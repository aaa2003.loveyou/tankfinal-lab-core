package com.tankfinal.lab.core.http.core;

import com.tankfinal.lab.core.exception.constant.Systems;
import com.tankfinal.lab.core.exception.customize.SystemException;
import com.tankfinal.lab.core.http.proxy.ProxyConfig;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpStatus;

import java.io.IOException;

/**
 * Http 核心處理邏輯
 *
 * @author Tank.Yang
 * @since 2020-06-18 11:24:34
 */
@Slf4j
@Getter
@Setter
@Builder
public class HttpCore {

    /**
     * Http Socket 超時時間
     */
    private int timeout;
    /**
     * 代理配置
     */
    private ProxyConfig proxyConfig;
    /**
     * 請求錯誤
     */
    private static final int ERROR = -1;
    /**
     * 默認的config配置
     */
    private static final int TIMEOUT = 15000;
    /**
     * 認證
     */
    private static final String AUTHORIZATION = "Authorization";

    /**
     * 認證
     */
    private static final String JWT_PREFIX = "Bearer ";

    /**
     * 效能監測印出字符串
     */
    private static String PERFORMANCE_LOGGER_STR = "== 請求%s完成 耗时:%sms ==";
    /**
     * 預設字符編碼
     */
    public static String DEFAULT_CHARSET = "UTF-8";
    /**
     * 預設請求投傳輸類型
     */
    public static String DEFAULT_CONTENT_TYPE = "application/json";

    /**
     * 交握處理
     *
     * @param method              請求方式及其內容
     * @param authorizationHeader 認證請求頭
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-22 20:09:35
     */
    public Entity hand(final HttpRequestBase method, String authorizationHeader) {
        method.setConfig(this.config());
        if (StringUtils.isNotEmpty(authorizationHeader)) {

            if (!authorizationHeader.contains(JWT_PREFIX)) {
                authorizationHeader = JWT_PREFIX + authorizationHeader;
            }
            method.addHeader(AUTHORIZATION, authorizationHeader);
        }
        // 執行對應的請求
        try (CloseableHttpClient client = HttpClients.custom().build(); CloseableHttpResponse resp = client.execute(method)) {
            long startTime = System.currentTimeMillis();
            Entity entity = Entity
                    .builder()
                    .statusCode(resp.getStatusLine().getStatusCode())
                    .result(EntityUtils.toString(resp.getEntity(), DEFAULT_CHARSET))
                    .build();
            // 檢測返回碼是否為Common HTTP status系列錯誤
            if (hasError(entity)) {
                throw new SystemException(Systems.HttpEx.HTTP_REQUEST_FAILED, String.valueOf(entity.getStatusCode()), entity.getResult());
            }
            log.info(String.format(PERFORMANCE_LOGGER_STR, method.getURI(), (System.currentTimeMillis() - startTime)));
            return entity;
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return Entity
                    .builder()
                    .statusCode(ERROR)
                    .result(e.getMessage())
                    .build();
        }
    }

    /**
     * 是否為伺服器錯誤
     *
     * @param entity HTTP Core Entity
     * @return boolean
     * @author Tank.Yang
     * @since 2020-07-08 16:05:13
     */
    private boolean hasError(Entity entity) {
        HttpStatus statusCode = HttpStatus.resolve(entity.getStatusCode());
        return (statusCode != null && hasError(statusCode));
    }

    /**
     * 檢測狀態碼是否為Common HTTP status series
     *
     * @param statusCode 狀態碼
     * @return boolean
     * @author Tank.Yang
     * @since 2020-07-08 16:05:16
     */
    private boolean hasError(HttpStatus statusCode) {
        return (statusCode.series() == HttpStatus.Series.SERVER_ERROR
                || statusCode.series() == HttpStatus.Series.CLIENT_ERROR && statusCode.value() != HttpStatus.UNAUTHORIZED.value());
    }

    /**
     * 構建配置信息
     *
     * @return org.apache.http.client.config.RequestConfig
     * @author Tank.Yang
     * @since 2020-06-18 11:15:06
     */
    private RequestConfig config() {
        int timeout = this.timeout <= 0 ? TIMEOUT : this.timeout;

        RequestConfig.Builder config = RequestConfig
                .custom()
                .setConnectionRequestTimeout(timeout)
                .setConnectTimeout(timeout)
                .setSocketTimeout(timeout);

        // 是否配置了代理
        if (null != proxyConfig) {
            if (StringUtils.isBlank(proxyConfig.getHost())) {
                throw new SystemException(Systems.HttpEx.PROXY_HOST_CANNOT_BE_EMPTY);
            }
            if (proxyConfig.getPort() <= 0) {
                throw new SystemException(Systems.HttpEx.PROXY_PORT_CANNOT_BE_EMPTY);
            }
            // 配置代理信息
            config.setProxy(new HttpHost(proxyConfig.getHost(), proxyConfig.getPort()));
        }
        return config.build();
    }

}
