package com.tankfinal.lab.core.http;

import com.tankfinal.lab.core.http.core.Entity;
import com.tankfinal.lab.core.http.core.HttpCore;
import com.tankfinal.lab.core.http.proxy.ProxyConfig;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.core.io.FileSystemResource;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.IntStream;

/**
 * Http工具類
 *
 * @author Tank.Yang
 * @since 2020-06-18 11:07:31
 */
public class HttpUtil {

    /**
     * 發起Http Get請求
     *
     * @param url                 請求地址
     * @param authorizationHeader 認證Header(jwt)
     * @return Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:07:55
     */
    public static Entity get(String url, String authorizationHeader) {
        return HttpCore
                .builder()
                .build()
                .hand(new HttpGet(url), authorizationHeader);
    }

    /**
     * 發起Http Get請求
     *
     * @param url                 請求地址
     * @param authorizationHeader 認證Header(jwt)
     * @param timeout             超時時間
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:08:15
     */
    public static Entity get(String url, String authorizationHeader, int timeout) {
        return HttpCore
                .builder()
                .timeout(timeout)
                .build()
                .hand(new HttpGet(url), authorizationHeader);
    }

    /**
     * 發起代理的Http Get請求
     *
     * @param url                 請求地址
     * @param proxyConfig         代理配置
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:08:33
     */
    public static Entity proxyGet(String url, ProxyConfig proxyConfig, String authorizationHeader) {
        return HttpCore
                .builder()
                .proxyConfig(proxyConfig)
                .build()
                .hand(new HttpGet(url), authorizationHeader);
    }

    /**
     * 發起代理的Http Get請求
     *
     * @param url                 請求地址
     * @param timeout             超时时间
     * @param proxyConfig         代理配置
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:08:51
     */
    public static Entity proxyGet(String url, int timeout, ProxyConfig proxyConfig, String authorizationHeader) {
        return HttpCore
                .builder()
                .timeout(timeout)
                .proxyConfig(proxyConfig)
                .build()
                .hand(new HttpGet(url), authorizationHeader);
    }

    /**
     * 發起Http POST 請求
     *
     * @param url                 請求地址
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:01
     */
    public static Entity post(String url, String authorizationHeader) {
        HttpPost post = new HttpPost(url);
        post.setEntity(EntityBuilder.create().setParameters().build());
        return HttpCore
                .builder()
                .build()
                .hand(new HttpPost(url), authorizationHeader);
    }

    /**
     * 發起Http POST 請求
     *
     * @param url                 請求地址
     * @param timeout             超时时间
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:13
     */
    public static Entity post(String url, int timeout, String authorizationHeader) {
        return HttpCore
                .builder()
                .timeout(timeout)
                .build()
                .hand(new HttpPost(url), authorizationHeader);
    }

    /**
     * 發起Http POST 請求
     *
     * @param url                 請求地址
     * @param params              請求参数
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:30
     */
    public static Entity post(String url, Map<String, String> params, String authorizationHeader) {
        final HttpPost post = new HttpPost(url);

        HttpEntity entity = EntityBuilder
                .create()
                .setParameters(create(params))
                .build();

        post.setEntity(entity);

        return HttpCore
                .builder()
                .build()
                .hand(post, authorizationHeader);
    }

    /**
     * 發起Http POST 請求
     *
     * @param url                 請求地址
     * @param param               請求參數
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:30
     */
    public static Entity post(String url, String param, String authorizationHeader) {
        final HttpPost post = new HttpPost(url);
        try {
            StringEntity entity = new StringEntity(param, "UTF-8");
            entity.setContentEncoding(HttpCore.DEFAULT_CHARSET);
            entity.setContentType(HttpCore.DEFAULT_CONTENT_TYPE);
            post.setEntity(entity);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return HttpCore
                .builder()
                .build()
                .hand(post, authorizationHeader);
    }


    /**
     * 發起Http POST 請求
     *
     * @param url                 請求地址
     * @param param               請求參數
     * @param authorizationHeader 認證Header(jwt)
     * @param timeout             逾時時間
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:30
     */
    public static Entity post(String url, String param, String authorizationHeader, int timeout) {
        final HttpPost post = new HttpPost(url);
        try {
            StringEntity entity = new StringEntity(param, "UTF-8");
            entity.setContentEncoding(HttpCore.DEFAULT_CHARSET);
            entity.setContentType(HttpCore.DEFAULT_CONTENT_TYPE);
            post.setEntity(entity);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return HttpCore
                .builder()
                .timeout(timeout)
                .build()
                .hand(post, authorizationHeader);
    }

    /**
     * 發起Http PUT 請求
     *
     * @param url                 請求地址
     * @param param               請求參數
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:30
     */
    public static Entity put(String url, String param, String authorizationHeader) {
        final HttpPut put = new HttpPut(url);
        try {
            StringEntity entity = new StringEntity(param, "UTF-8");
            entity.setContentEncoding(HttpCore.DEFAULT_CHARSET);
            entity.setContentType(HttpCore.DEFAULT_CONTENT_TYPE);
            put.setEntity(entity);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return HttpCore
                .builder()
                .build()
                .hand(put, authorizationHeader);
    }

    /**
     * 發起Http PUT 請求
     *
     * @param url                 請求地址
     * @param param               請求參數
     * @param authorizationHeader 認證Header(jwt)
     * @param timeout             逾時時間
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:30
     */
    public static Entity put(String url, String param, String authorizationHeader, int timeout) {
        final HttpPut put = new HttpPut(url);
        try {
            StringEntity entity = new StringEntity(param, "UTF-8");
            entity.setContentEncoding(HttpCore.DEFAULT_CHARSET);
            entity.setContentType(HttpCore.DEFAULT_CONTENT_TYPE);
            put.setEntity(entity);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return HttpCore
                .builder()
                .timeout(timeout)
                .build()
                .hand(put, authorizationHeader);
    }

    /**
     * 發起Http Patch 請求
     *
     * @param url                 請求地址
     * @param param               請求參數
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:30
     */
    public static Entity patch(String url, String param, String authorizationHeader) {
        final HttpPatch patch = new HttpPatch(url);
        try {
            StringEntity entity = new StringEntity(param, "UTF-8");
            entity.setContentEncoding(HttpCore.DEFAULT_CHARSET);
            entity.setContentType(HttpCore.DEFAULT_CONTENT_TYPE);
            patch.setEntity(entity);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return HttpCore
                .builder()
                .build()
                .hand(patch, authorizationHeader);
    }

    /**
     * 發起Http Patch 請求
     *
     * @param url                 請求地址
     * @param param               請求參數
     * @param authorizationHeader 認證Header(jwt)
     * @param timeout             逾時時間
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:30
     */
    public static Entity patch(String url, String param, String authorizationHeader, int timeout) {
        final HttpPatch patch = new HttpPatch(url);
        try {
            StringEntity entity = new StringEntity(param, "UTF-8");
            entity.setContentEncoding(HttpCore.DEFAULT_CHARSET);
            entity.setContentType(HttpCore.DEFAULT_CONTENT_TYPE);
            patch.setEntity(entity);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return HttpCore
                .builder()
                .timeout(timeout)
                .build()
                .hand(patch, authorizationHeader);
    }

    /**
     * 發起Http Patch請求
     *
     * @param url                 請求地址
     * @param authorizationHeader 認證Header(jwt)
     * @return Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:07:55
     */
    public static Entity patch(String url, String authorizationHeader) {
        return HttpCore
                .builder()
                .build()
                .hand(new HttpPatch(url), authorizationHeader);
    }

    /**
     * 發起Http POST 請求
     *
     * @param url                 請求地址
     * @param uploadFiles         上傳檔案
     * @param fileNames           檔案對應到的接口參數名稱
     * @param params              請求參數
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:30
     */
    public static Entity post(String url, String authorizationHeader, List<FileSystemResource> uploadFiles, List<String> fileNames, Map<String, Object> params) {
        final HttpPost post = new HttpPost(url);
        post.setEntity(createFileEntity(uploadFiles, fileNames, params));
        return HttpCore
                .builder()
                .build()
                .hand(post, authorizationHeader);
    }

    /**
     * 發起Http POST 請求(檔案適用)
     *
     * @param url                 請求地址
     * @param params              請求參數
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:30
     */
    public static Entity post(String url, String authorizationHeader, Map<String, Object> params) {
        final HttpPost post = new HttpPost(url);
        post.setEntity(createFileEntity(null, null, params));
        return HttpCore
                .builder()
                .build()
                .hand(post, authorizationHeader);
    }

    /**
     * 發起Http POST 請求(檔案適用)
     *
     * @param url                 請求地址
     * @param params              請求參數
     * @param authorizationHeader 認證Header(jwt)
     * @param timeout             逾時時間
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:30
     */
    public static Entity postWithMap(String url, String authorizationHeader, Map<String, Object> params, int timeout) {
        final HttpPost post = new HttpPost(url);
        post.setEntity(createFileEntity(null, null, params));
        return HttpCore
                .builder()
                .timeout(timeout)
                .build()
                .hand(post, authorizationHeader);
    }


    /**
     * 發起Http POST 請求
     *
     * @param url                 請求地址
     * @param uploadFiles         上傳檔案
     * @param fileNames           檔案對應到的接口參數名稱
     * @param params              請求參數
     * @param authorizationHeader 認證Header(jwt)
     * @param timeout             逾時時間
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:30
     */
    public static Entity post(String url, String authorizationHeader, List<FileSystemResource> uploadFiles, List<String> fileNames, Map<String, Object> params, int timeout) {
        final HttpPost post = new HttpPost(url);
        post.setEntity(createFileEntity(uploadFiles, fileNames, params));
        return HttpCore
                .builder()
                .timeout(timeout)
                .build()
                .hand(post, authorizationHeader);
    }

    /**
     * 發起Http POST 請求
     *
     * @param url                 請求地址
     * @param timeout             超时时间
     * @param params              請求参数
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:43
     */
    public static Entity post(String url, int timeout, Map<String, String> params, String authorizationHeader) {
        final HttpPost post = new HttpPost(url);

        HttpEntity entity = EntityBuilder
                .create()
                .setParameters(create(params))
                .build();

        post.setEntity(entity);
        return HttpCore
                .builder()
                .timeout(timeout)
                .build()
                .hand(post, authorizationHeader);
    }

    /**
     * 通過代理發起Http POST 請求
     *
     * @param url                 請求地址
     * @param proxyConfig         代理配置
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:09:59
     */
    public static Entity proxyPost(String url, ProxyConfig proxyConfig, String authorizationHeader) {

        return HttpCore
                .builder()
                .proxyConfig(proxyConfig)
                .build()
                .hand(new HttpPost(url), authorizationHeader);
    }

    /**
     * 通過代理發起Http POST 請求
     *
     * @param url                 請求地址
     * @param proxyConfig         代理配置
     * @param params              請求参数
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:10:38
     */
    public static Entity proxyPost(String url, ProxyConfig proxyConfig, Map<String, String> params, String authorizationHeader) {
        final HttpPost post = new HttpPost(url);

        HttpEntity entity = EntityBuilder
                .create()
                .setParameters(create(params))
                .build();

        post.setEntity(entity);

        return HttpCore
                .builder()
                .proxyConfig(proxyConfig)
                .build()
                .hand(post, authorizationHeader);
    }

    /**
     * 通過代理發起Http POST 請求
     *
     * @param url                 請求地址
     * @param timeout             超时时间
     * @param proxyConfig         代理配置
     * @param params              請求参数
     * @param authorizationHeader 認證Header(jwt)
     * @return com.tankfinal.core.util.http.core.Entity
     * @author Tank.Yang
     * @since 2020-06-18 11:10:54
     */
    public static Entity proxyPost(String url, int timeout, ProxyConfig proxyConfig, Map<String, String> params, String authorizationHeader) {
        final HttpPost post = new HttpPost(url);

        HttpEntity entity = EntityBuilder
                .create()
                .setParameters(create(params))
                .build();

        post.setEntity(entity);

        return HttpCore
                .builder()
                .timeout(timeout)
                .proxyConfig(proxyConfig)
                .build()
                .hand(post, authorizationHeader);
    }

    /**
     * 將Map参数轉為POST提交參數列表
     *
     * @param params 請求參數
     * @return java.util.List<org.apache.http.NameValuePair>
     * @author Tank.Yang
     * @since 2020-06-18 11:11:21
     */
    public static List<NameValuePair> create(Map<String, String> params) {
        List<NameValuePair> parameters = new ArrayList<>(params.size());
        params.forEach((k, v) -> parameters.add(new BasicNameValuePair(k, v)));
        return parameters;
    }

    /**
     * 將File相關參數轉為HttpEntity
     *
     * @param uploadFiles 上傳的檔案
     * @param fileNames   key值
     * @param params      附加參數
     * @return org.apache.http.HttpEntity
     * @author Tank.Yang
     * @since 2020-07-15 13:39:48
     */
    public static HttpEntity createFileEntity(List<FileSystemResource> uploadFiles, List<String> fileNames, Map<String, Object> params) {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setCharset(StandardCharsets.UTF_8);
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        if (CollectionUtils.isNotEmpty(uploadFiles) || CollectionUtils.isNotEmpty(fileNames)) {
            if (uploadFiles.size() == fileNames.size()) {
                IntStream.range(0, uploadFiles.size()).forEach(idx -> {
                    try {
                        // 文件傳輸http请求頭(multipart/form-data)
                        builder.addBinaryBody(fileNames.get(idx), uploadFiles.get(idx).getInputStream(), ContentType.MULTIPART_FORM_DATA,
                                uploadFiles.get(idx).getFilename());
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                });
            }
        }

        // 字節傳輸http请求頭(application/json)
        ContentType contentType = ContentType.create(HttpCore.DEFAULT_CONTENT_TYPE, StandardCharsets.UTF_8);
        // 非空判斷
        if (MapUtils.isNotEmpty(params)) {
            // 字節流
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                if (Objects.nonNull(entry.getValue())) {
                    try {
                        // 檔案
                        if (entry.getValue() instanceof Collection) {
                            Collection<?> collection = (Collection) entry.getValue();
                            for (Object obj : collection) {
                                if (obj instanceof FileSystemResource) {
                                    FileSystemResource fileSystemResource = (FileSystemResource) obj;
                                    // 文件傳輸http请求頭(multipart/form-data)
                                    builder.addBinaryBody(entry.getKey(), fileSystemResource.getInputStream(), ContentType.MULTIPART_FORM_DATA, fileSystemResource.getFilename());
                                }
                            }
                        } else if (entry.getValue() instanceof FileSystemResource) {
                            FileSystemResource fileSystemResource = (FileSystemResource) entry.getValue();
                            // 文件傳輸http请求頭(multipart/form-data)
                            builder.addBinaryBody(entry.getKey(), fileSystemResource.getInputStream(), ContentType.MULTIPART_FORM_DATA, fileSystemResource.getFilename());
                        }
                        // 非檔案
                        else {
                            String key = entry.getKey();
                            String value = entry.getValue().toString();
                            builder.addTextBody(key, value, contentType);
                        }
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        return builder.build();
    }
}
